# checkframes

## Overview
"checkframes" is a Render-QC tool that 

- detects image-sequence frames that have problems (missing and/or corrupt)

- optionally fills in missing frames and replaces corrupt frames using
suitable adjacent frames
- 
## Notes for this DEMO version (checkframes_demo)
- there is no installer (normally CICD... ".gitlab-ci.yml" and "build.py" file not included)
- the code to detect black-frames (using ffmpeg) is still in dev and not included

## USAGE

There are THREE modes of operation:
1. GUI driven tool
2. Library
3. CLUI driven tool (command-line tool)

The GUI provides user control of parameters used for corrupt-file detection;  otherwise,
these parameters have default values, set in the configuration file

As a library, the code can be called from other tools (e.g. render-publishers)

The CLUI is useful for subprocessing and parallelization

### GUI driven tool
launched via CLUI
```shell script
> checkframes -g
```
launched from a Windows CMD file
```shell script
start /b python <insert-installed-path-to>\checkframes\launcher.pyc -g %*
```


### Library API
```python
from checkframes import check_frames

input_data = {
    "directory": r"<path-to-directory-containing-image-sequence>",
    "frange": ["1001", "1110"],
    "gui": False,
}

check_frames(input_data)
```

### CLUI
```shell script
> checkframes -h

usage: checkframes [-h] [-d DIRECTORY] [-f FRANGE FRANGE] [-g]

Creates image files for dropped frames in the image sequence at the given
directory

optional arguments:
  -h, --help            show this help message and exit
  -d DIRECTORY, --directory DIRECTORY
                        specify image-sequence directory e.g. "U:/dropbox/fabl
                        es/20201105_02/FB101_122_000_002_0I01_PC002_Z1_AV001"
  -f FRANGE FRANGE, --frange FRANGE FRANGE
                        frame-range ; e.g. "-f 0 1000", "-f 0000 0100"
  -g, --gui             Run with a GUI
```

### Overview of Code Organization 

```
checkframes
├── README.md
├── bin
├── etc
└── src
    ├── data
    ├── domain
    ├── presentation
    ├── test
    ├── config.py
    ├── environment.py
    └── launcher.py
```
### Entry Point
"launcher.py"  
   - sets up the tool-specific runtime environment
   - determines the runtime-mode (e.g. GUI, API, CLUI)
   - assembles the runtime-code ("connects" presentation, domain and data layers)
   - launches the runtime in the appropriate mode
### CORE CODE
Organized in 3 layers and follows an "Hexagonal" architecture
#### domain
- the domain-layer implements the core actions of the tool.
```
└── src
    ├── domain
    │   ├── corrupt_frames.py
    │   ├── fix_frames.py
    │   ├── missing_frames.py
  ```
#### presentation  
- the Presentation Layer implemements a  Model-View-Presenter pattern
  - the VIEW can be either Command-Line-UI [clui],or Graphical-UI [gui]
  - the GUI implements a Model-View-Presenter with Passive-View (no logic)
```
└── src
    ├── presentation
    │   ├── checkframes_clui.py
    │   └── checkframes_gui.py
```

#### data
- code here provides access to external data sources/storage (e.g. databases, filesystem etc.)
