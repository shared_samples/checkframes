"""
Bootstrap application environment
RUN CLUI, GUI or API
"""

# STANDARD Imports
import os

# PACKAGE Imports
try:
    from src.environment import set_tool_runtime_environment
    from src.domain.checkframes import checkframes
    from src.domain.checkframes import get_sequence_frange_data
    from src.presentation import checkframes_clui
    from src.presentation import checkframes_gui
    from src.config import get_config
except ModuleNotFoundError:  # for tests
    from environment import set_tool_runtime_environment
    from domain.checkframes import checkframes
    from domain.checkframes import get_sequence_frange_data
    from presentation import checkframes_clui
    from presentation import checkframes_gui
    from config import get_config


def _get_sequence_frange_data(sequence_directory, config):
    """Exposes get_sequence_frange_data to external packages (e.g. VCAMP)
    via API defined in checkframes.__init__()

    Note: checkframes_gui does not call this, it calls
    get_sequence_frange_data directly
    Args:
        sequence_directory:

    """
    return get_sequence_frange_data(sequence_directory, config)


def launch(
    directory=None, frange=None, config=None
):  # pylint: disable=R1710  # inconsistent-return-statements
    """TODO replace input_data with explicit key-words
    Launches with CLUI, GUI or API

    Args:
        directory (string): only used with API (directory value passed from colling Tool)
        frange (list):  only used with API
    """
    # BOOTSTRAP
    set_tool_runtime_environment()

    if not config:
        config = get_config(os.environ["CFS_PACKAGE_CONFIG"])

    # DETERMINE the INTERFACE Type being used
    running_gui = False
    running_clui = False  # for running as sub-processes
    running_api = False

    if directory:
        running_api = True
    else:
        input_data = checkframes_clui.get_command_line_arguments()
        running_gui = input_data["gui"]
        if not running_gui:
            running_clui = True

    # CONTINUE using the appropriate INTERFACE
    if running_gui:  # results returned to GUI
        domain_api = {
            "checkframes_controller": checkframes,
            "get_sequence_frange_data": get_sequence_frange_data,
        }
        checkframes_gui.run_checkframes_gui(config=config, domain_api=domain_api)

    elif running_api:  # results returned to CALLER
        results = checkframes(
            config, directory=directory, frange=frange, running_api=True
        )
        if results:
            return results  # inconsistent-return-statements

    elif running_clui:  # results returned to CLUI
        reply_method = checkframes_clui.report_results
        checkframes(config, **input_data, reply_method=reply_method)


if __name__ == "__main__":
    # When running from PyCharm (e.g. during dev)
    # add "-g" to the launch configuration, otherwise this launches CLUI
    # ( the bin/ cmd file has the "-g" switch as well)
    launch()
