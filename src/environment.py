# pylint: disable=E0401  # import-error
# pylint: disable=C0415  # Import outside toplevel
"""
Bootstrap application environment
"""

# STANDARD IMPORTS
import os
import sys
import pathlib


def _get_package_roots():
    """
    Gets absolute paths to package SOURCE and CONFIG locations at runtime
    - based on location of THIS file in the package
      AND a priori knowledge of package structure (ugly, but this is bootstrapping)

    Returns:
       src_root (string): Absolute path to the SOURCE root for this tool.
           - this later gets added to sys.path

       cfg_root: (string): Absolute path to the location of the
                           configuration file for this tool
           e.g. ".../checkframes/etc"


    """
    this_path = pathlib.Path(__file__)
    src_root = this_path.parent

    pkg_root = src_root.parents[0]
    #     pkg_root = src_root.parents[<index>]
    #    <index> is dependent on the location of THIS file in the package structure
    # "pkg_root"  corresponds to a GIT PROJECT

    cfg_root = pkg_root.joinpath("etc")

    return src_root, cfg_root


def set_tool_runtime_environment():
    """
    "CFS" - CheckFrameS
    - set environment variables
    - add SOURCE directory to sys.path
    """
    src_root, cfg_root = _get_package_roots()

    pkg_cfg_path = cfg_root.joinpath("checkframes_configuration.yml")
    tst_resources = cfg_root.joinpath("tst_resources")
    icons_folder = cfg_root.joinpath("icons")

    os.environ["CFS_PACKAGE_RESOURCES"] = str(cfg_root)
    os.environ["CFS_PACKAGE_CONFIG"] = str(pkg_cfg_path)
    os.environ["CFS_PACKAGE_ICONS"] = str(icons_folder)
    os.environ["CFS_TEST_RESOURCES"] = str(tst_resources)

    sys.path.append(os.path.join(*src_root.parts))


if __name__ == '__main__':
    src_root, cfg_root = _get_package_roots()
    print(f"{src_root}\n{cfg_root}")
