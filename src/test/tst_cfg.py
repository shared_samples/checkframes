"""


"""
import os
import pathlib
import sys
from pprint import pprint


def _get_package_roots():
    this_path = pathlib.Path(__file__)
    src_root = this_path.parents[1]
    cfg_root = this_path.parents[2].joinpath("etc")

    return src_root, cfg_root


def _set_package_env():
    """
    TODO:
    Returns:

    """
    src_root, cfg_root = _get_package_roots()
    print(f"{src_root}\n{cfg_root}")
    pkg_cfg_path = cfg_root.joinpath("checkframes_configuration.yml")
    os.environ["CFS_PACKAGE_CONFIG"] = str(pkg_cfg_path)
    os.environ["CFS_PACKAGE_RESOURCES"] = str(cfg_root)

    sys.path.append(os.path.join(*src_root.parts))


_set_package_env()

from config import get_config

config = get_config(os.environ["CFS_PACKAGE_CONFIG"])
