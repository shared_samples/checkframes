# pylint: disable=E0401  # import-error
"""

"""
# STANDARD LIB
import os
import re
from pprint import pprint
from pytest import mark

# PACKAGE IMPORTS
try:
    from tst_cfg import config as CONFIG
    from domain import checkframes
    from domain import corrupt_frames
    from domain import missing_frames
    from domain import fix_frames
except ModuleNotFoundError:
    from src.test.tst_cfg import config as CONFIG
    from src.domain import checkframes
    from src.domain import corrupt_frames
    from src.domain import missing_frames
    from src.domain import fix_frames
# pprint(CONFIG)
CFS_PACKAGE_RESOURCES = os.environ["CFS_PACKAGE_RESOURCES"]
# FRAME_REGEX = re.compile(r"{}".format(CONFIG["image_frame_filename_pattern"]))
EXPECTED_FRANGE = ["1001", "1010"]


@mark.parametrize(
    "sorted_sequence_filenames_test_case, expected_result",
    [
        ("drop_all", []),
        (
            "drop_first3_last3",
            [
                "image_file.1004.png",
                "image_file.1005.png",
                "image_file.1006.png",
                "image_file.1007.png",
            ],
        ),
    ],
)
def test_get_sorted_sequence_filenames_finds_files(
    sorted_sequence_filenames_test_case, expected_result, pkg_config
):
    """
    get seq_dir that points to a test sequence
    """
    seq_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", sorted_sequence_filenames_test_case
    )
    result = checkframes._get_sorted_sequence_filenames(seq_dir, pkg_config)

    assert result["sequence_filenames"] == expected_result


@mark.parametrize(
    "sorted_sequence_filenames_errors_test_case, expected_result",
    [
        ("drop_all", True),
        ("drop_first3_last3", False),
    ],
)
def test_get_sorted_sequence_filenames_returns_errors(
    sorted_sequence_filenames_errors_test_case, expected_result, pkg_config
):
    """
    get seq_dir that points to a test sequence
    """
    seq_dir = os.path.join(
        CFS_PACKAGE_RESOURCES,
        "tst_resources",
        sorted_sequence_filenames_errors_test_case,
    )
    result = checkframes._get_sorted_sequence_filenames(seq_dir, pkg_config)

    assert bool(result["errors"]) == expected_result


# def test_get_sorted_sequence_filenames_finds_files(pkg_config):
#     """
#     get seq_dir that points to a test sequence
#     result = {"sequence_filenames": [], "errors": []}
#     """
#     seq_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "drop_first3_last3")
#     result = checkframes._get_sorted_sequence_filenames(seq_dir, pkg_config)
#     assert result["errors"] == []
#     assert result["sequence_filenames"] == [
#         "image_file.1004.png",
#         "image_file.1005.png",
#         "image_file.1006.png",
#         "image_file.1007.png",
#     ]


def test_get_sorted_sequence_filenames_reports_no_files_found(pkg_config):
    """
    get seq_dir that points to a test sequence
    """
    seq_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "drop_all")
    result = checkframes._get_sorted_sequence_filenames(seq_dir, pkg_config)

    expected = f"Error: No image files found at: {seq_dir}\n"
    expected += "Please select a directory containing an image sequence"

    assert result["errors"][0] == expected


def test_get_frame_numbers_finds_frame_numbers(pkg_config):
    seq_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "drop_first3_last3")

    get_filenames_result = checkframes._get_sorted_sequence_filenames(
        seq_dir, pkg_config
    )
    filenames = get_filenames_result["sequence_filenames"]

    result = missing_frames._get_existing_frame_numbers(filenames, pkg_config)

    assert result == ["1004", "1005", "1006", "1007"]


# ######################
# MISSING FRAMES
# ######################


# def test_get_missing_frame_data_returns_full_list_when_no_frames_present():
"""_get_missing_frame_data is not called, if no frames are present"""


@mark.parametrize(
    "missing_frames_test_case, expected_result",
    [
        ("drop_none", []),
        ("drop_internal_1frame", [(1, "1006")]),
        ("drop_internal_isolated_2span_5span", [(2, "1001"), (5, "1004")]),
        ("drop_first1_last1", [(1, "1009"), (-1, "1002")]),
        ("drop_first3_last3", [(3, "1007"), (-3, "1004")]),
    ],
)
def test_get_missing_frame_data(missing_frames_test_case, expected_result, pkg_config):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", missing_frames_test_case
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    missing_frame_data = missing_frames.get_missing_frame_data(
        ret_val["sequence_filenames"], EXPECTED_FRANGE, pkg_config
    )
    result = missing_frame_data["frame_fill_spec"]
    assert result == expected_result


# NON-PARAMERTERIZED versions of "test_get_missing_frame_data" -
# def test_get_missing_frame_data_returns_empty_list_when_all_frames_present(pkg_config):
#     seq_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "drop_none")
#     get_filenames_result = checkframes._get_sorted_sequence_filenames(
#         seq_dir, pkg_config
#     )
#     filenames = get_filenames_result["sequence_filenames"]
#
#     missing_frame_data = missing_frames.get_missing_frame_data(
#         filenames, EXPECTED_FRANGE, pkg_config
#     )
#     result = missing_frame_data["frame_fill_spec"]
#     assert result == []
#
def test_get_missing_frame_data_internal_1frame(pkg_config):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_internal_isolated_2frames_AND_drop_internal_1frame"
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    missing_frame_data = missing_frames.get_missing_frame_data(
        ret_val["sequence_filenames"], EXPECTED_FRANGE, pkg_config
    )
    pprint(missing_frame_data)
    result = missing_frame_data["frame_fill_spec"]
    print(result)
    #assert result == [(1, "1006")]
#
#
# def test_get_missing_frame_data_internal_isolated_2span_5span(pkg_config):
#     src_dir = os.path.join(
#         CFS_PACKAGE_RESOURCES, "tst_resources", "drop_internal_isolated_2span_5span"
#     )
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     missing_frame_data = missing_frames.get_missing_frame_data(
#         ret_val["sequence_filenames"], EXPECTED_FRANGE, pkg_config
#     )
#     # pprint(missing_frame_data)
#     result = missing_frame_data["frame_fill_spec"]
#     assert result == [(2, "1001"), (5, "1004")]
#
#
# def test_get_missing_frame_data_drop_first1_last1(pkg_config):
#     src_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "drop_first1_last1")
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     missing_frame_data = missing_frames.get_missing_frame_data(
#         ret_val["sequence_filenames"], EXPECTED_FRANGE, pkg_config
#     )
#     # pprint(missing_frame_data)
#     result = missing_frame_data["frame_fill_spec"]
#     assert result == [(1, "1009"), (-1, "1002")]
#
#
# def test_get_missing_frame_data_drop_first3_last3(pkg_config):
#     src_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "drop_first3_last3")
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     missing_frame_data = missing_frames.get_missing_frame_data(
#         ret_val["sequence_filenames"], EXPECTED_FRANGE, pkg_config
#     )
#     print(missing_frame_data)
#     result = missing_frame_data["frame_fill_spec"]
#     assert result == [(3, "1007"), (-3, "1004")]

# ######################
# CORRUPT FRAMES
# ######################


#
@mark.parametrize(
    "corrupt_frame_filename_test_case, expected_result",
    [
        ("incomplete_none", {}),
        (
            "incomplete_first1_last1",
            {"image_file.1001.png": 10240, "image_file.1010.png": 10240},
        ),
        (
            "incomplete_first3_last3",
            {
                "image_file.1001.png": 10240,
                "image_file.1002.png": 10240,
                "image_file.1003.png": 10240,
                "image_file.1008.png": 10240,
                "image_file.1009.png": 10240,
                "image_file.1010.png": 10240,
            },
        ),
        (
            "incomplete_internal_5span",
            {
                "image_file.1003.png": 10240,
                "image_file.1004.png": 10240,
                "image_file.1005.png": 10240,
                "image_file.1006.png": 10240,
                "image_file.1007.png": 10240,
            },
        ),
        (
            "incomplete_internal_isolated_2frames",
            {
                "image_file.1003.png": 10240,
                "image_file.1004.png": 307200,
                "image_file.1005.png": 307200,
                "image_file.1006.png": 307200,
                "image_file.1007.png": 0,
                "image_file.1008.png": 307200,
                "image_file.1009.png": 307200,
                "image_file.1010.png": 307200,
            },
        ),
        (
            "incomplete_first_then_every_other",
            {
                "image_file.1001.png": 10240,
                "image_file.1003.png": 10240,
                "image_file.1005.png": 10240,
                "image_file.1007.png": 10240,
                "image_file.1009.png": 10240,
            },
        ),
        (
            "incomplete_all",
            {
                "image_file.1001.png": 10240,
                "image_file.1002.png": 10240,
                "image_file.1003.png": 10240,
                "image_file.1004.png": 10240,
                "image_file.1005.png": 10240,
                "image_file.1006.png": 10240,
                "image_file.1007.png": 10240,
                "image_file.1008.png": 10240,
                "image_file.1009.png": 10240,
                "image_file.1010.png": 10240,
            },
        ),
    ],
)
def test_get_corrupt_frame_filename(
    corrupt_frame_filename_test_case, expected_result, pkg_config
):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", corrupt_frame_filename_test_case
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    corrupt_frame_data = corrupt_frames.get_corrupt_frame_data(
        src_dir, ret_val["sequence_filenames"], pkg_config
    )
    result = corrupt_frame_data["corrupt_file_data"]
    assert result == expected_result


@mark.parametrize(
    "corrupt_frame_fill_spec_test_case, expected_result",
    [
        ("incomplete_none", []),
        ("incomplete_first1_last1", [(-1, "1002"), (1, "1009")]),
        ("incomplete_first3_last3", [(-3, "1004"), (3, "1007")]),
        ("incomplete_internal_5span", [(5, "1002")]),
        ("incomplete_internal_isolated_2frames", [(8, "1002")]),
        (
            "incomplete_first_then_every_other",
            [(-1, "1002"), (1, "1002"), (1, "1004"), (1, "1006"), (1, "1008")],
        ),
        ("incomplete_all", []),
    ],
)
def test_get_corrupt_frame_fill_spec(
    corrupt_frame_fill_spec_test_case, expected_result, pkg_config
):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", corrupt_frame_fill_spec_test_case
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    corrupt_frame_data = corrupt_frames.get_corrupt_frame_data(
        src_dir, ret_val["sequence_filenames"], pkg_config
    )
    result = corrupt_frame_data["frame_fill_data"]["frame_fill_spec"]

    assert result == expected_result


# @mark.parametrize(
#     "corrupt_frame_fill_filename_template_test_case, expected_result",
#     [
#         ("incomplete_none", ["image_file", "1001", "png"]),
#         ("incomplete_first1_last1", ["image_file", "1001", "png"]),
#         ("incomplete_first3_last3", ["image_file", "1001", "png"]),
#         ("incomplete_internal_5span", ["image_file", "1001", "png"]),
#         ("incomplete_internal_isolated_2frames", ["image_file", "1001", "png"]),
#         ("incomplete_first_then_every_other", ["image_file", "1001", "png"]),
#         ("incomplete_all", ["image_file", "1001", "png"]),
#     ],
# )
# def test_get_corrupt_frame_fill_filename_template(
#     corrupt_frame_fill_filename_template_test_case, expected_result, pkg_config
# ):
#     src_dir = os.path.join(
#         CFS_PACKAGE_RESOURCES,
#         "tst_resources",
#         corrupt_frame_fill_filename_template_test_case,
#     )
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#     corrupt_frame_data = corrupt_frames.get_corrupt_frame_data(
#         src_dir, ret_val["sequence_filenames"], pkg_config
#     )
#     result = corrupt_frame_data["frame_fill_data"]["filler_filename_template"]
#     assert result == expected_result


@mark.parametrize(
    "corrupt_frames_min_max_test_case, expected_result",
    [
        ("incomplete_none", [307200, 307200]),
        ("incomplete_first1_last1", [10240, 307200]),
        ("incomplete_first3_last3", [10240, 307200]),
        ("incomplete_internal_5span", [10240, 307200]),
        ("incomplete_internal_isolated_2frames", [0, 307200]),
        ("incomplete_first_then_every_other", [10240, 307200]),
        ("incomplete_all", [10240, 10240]),
    ],
)
def test_get_corrupt_frame_data(
    corrupt_frames_min_max_test_case, expected_result, pkg_config
):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", corrupt_frames_min_max_test_case
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    corrupt_frame_data = corrupt_frames.get_corrupt_frame_data(
        src_dir, ret_val["sequence_filenames"], pkg_config
    )
    result = corrupt_frame_data["min_max_file_size"]
    assert result == expected_result


@mark.parametrize(
    "corrupt_frame_max_delta_size, expected_result",
    [
        ("incomplete_none", [0, 0]),
        ("incomplete_first1_last1", [0, 97]),
        ("incomplete_first3_last3", [0, 97]),
        ("incomplete_internal_5span", [0, 97]),
        ("incomplete_internal_isolated_2frames", [0, 100]),
        ("incomplete_first_then_every_other", [0, 97]),
        ("incomplete_all", [0, 0]),
    ],
)
def test_get_corrupt_frame_max_delta_size(
    corrupt_frame_max_delta_size, expected_result, pkg_config
):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", corrupt_frame_max_delta_size
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    corrupt_frame_data = corrupt_frames.get_corrupt_frame_data(
        src_dir, ret_val["sequence_filenames"], pkg_config
    )
    result = corrupt_frame_data["min_max_percent_size_delta"]
    assert result == expected_result


# NON-PARAMERTERIZED versions of the previous parameretized tests:
#    "test_get_corrupt_frame_filename"
#    "test_get_corrupt_frame_fill_spec"
#    "test_get_corrupt_frame_data"
#    "test_get_corrupt_frame_max_delta_size"
#
# def test_get_corrupt_frame_data_none(pkg_config):
#     src_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_none")
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     frames_data = corrupt_frames.get_corrupt_frame_data(
#         src_dir, ret_val["sequence_filenames"], pkg_config
#     )
#     pprint(frames_data)
#
#     assert frames_data["frame_fill_data"]["frame_fill_spec"] == []
#     assert frames_data["corrupt_file_data"] == {}
#     assert frames_data["min_max_file_size"] == [307200, 307200]
#
#
# def test_get_corrupt_frame_data_first1_last1(pkg_config):
#     src_dir = os.path.join(
#         CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_first1_last1"
#     )
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     frames_data = corrupt_frames.get_corrupt_frame_data(
#         src_dir, ret_val["sequence_filenames"], pkg_config
#     )
#     pprint(frames_data)
#
#     assert frames_data["frame_fill_data"]["frame_fill_spec"] == [
#         (-1, "1002"),
#         (1, "1009"),
#     ]
#     assert frames_data["corrupt_file_data"] == {
#         "image_file.1001.png": 10240,
#         "image_file.1010.png": 10240,
#     }
#     assert frames_data["min_max_file_size"] == [10240, 307200]
#
#
# def test_get_corrupt_frame_data_first3_last3(pkg_config):
#     src_dir = os.path.join(
#         CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_first3_last3"
#     )
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     frames_data = corrupt_frames.get_corrupt_frame_data(
#         src_dir, ret_val["sequence_filenames"], pkg_config
#     )
#     pprint(frames_data)
#
#     assert frames_data["frame_fill_data"]["frame_fill_spec"] == [
#         (-3, "1004"),
#         (3, "1007"),
#     ]
#
#
# def test_get_corrupt_frame_data_internal_5span(pkg_config):
#     src_dir = os.path.join(
#         CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_internal_5span"
#     )
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     frames_data = corrupt_frames.get_corrupt_frame_data(
#         src_dir, ret_val["sequence_filenames"], pkg_config
#     )
#     pprint(frames_data)
#
#     assert frames_data["frame_fill_data"]["frame_fill_spec"] == [(5, "1002")]
#

def test_get_corrupt_frame_data_internal_isolated_2frames(pkg_config):
    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_internal_isolated_2frames"
    )
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)

    frames_data = corrupt_frames.get_corrupt_frame_data(
        src_dir, ret_val["sequence_filenames"], pkg_config
    )
    pprint(frames_data)

    assert frames_data["frame_fill_data"]["frame_fill_spec"] == [
        (1, "1002"),
        (1, "1006"),
    ]

#
# def test_get_corrupt_frame_data_first_then_every_other(pkg_config):
#     src_dir = os.path.join(
#         CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_first_then_every_other"
#     )
#     ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
#
#     frames_data = corrupt_frames.get_corrupt_frame_data(
#         src_dir, ret_val["sequence_filenames"], pkg_config
#     )
#     pprint(frames_data)
#
#     assert frames_data["frame_fill_data"]["frame_fill_spec"] == [
#         (-1, "1002"),
#         (1, "1002"),
#         (1, "1004"),
#         (1, "1006"),
#         (1, "1008"),
#     ]
#
#
def test_get_corrupt_frame_data_all(pkg_config):
    """
    uses, from CONFIG: min_size_bytes: 204800   (200kB)
    """
    src_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_all")
    ret_val = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)

    frames_data = corrupt_frames.get_corrupt_frame_data(
        src_dir, ret_val["sequence_filenames"], pkg_config
    )
    pprint(frames_data)
    assert len(frames_data["corrupt_file_data"].keys()) == 10


#
# def test_get_existing_frange_returns_error_on_bad_framenumbers(pkg_config):
#     src_dir = os.path.join(CFS_PACKAGE_RESOURCES, "tst_resources", "bad_framenumbers")
#     result = checkframes.get_sequence_frange_data(src_dir, pkg_config)
#     pprint(result)
#     assert "Error" in result



def test_fill_drop_frames_fills_missing_and_corrupt_frames(image_frame_regex, expected_frange, pkg_config):
    expected_frange = ["0000", "0009"]

    src_dir = os.path.join(
        CFS_PACKAGE_RESOURCES, "tst_resources", "incomplete_internal_3span_and_first3_drop"
    )
    img_seq_filenames = checkframes._get_sorted_sequence_filenames(src_dir, pkg_config)
    pprint(img_seq_filenames)
    #frames_partitioned_by_size = corrupt_frames.get_frame_size_data(src_dir, img_seq)
    frames_partitioned_by_size = corrupt_frames.get_corrupt_frame_data(src_dir, img_seq_filenames, pkg_config)
    files_to_drop = [
        os.path.basename(f) for f in frames_partitioned_by_size["incomplete"].keys()
    ]
    src_dst_filenames, missing_frames_warnings = missing_frames.fill_missing_frames(
        src_dir, img_seq_filenames, expected_frange, pkg_config, dry_run=True
    )
    assert missing_frames_warnings == []
    assert src_dst_filenames == {
        'image_file.0000.png': ['image_file.0001.png'],
        'image_file.0002.png': ['image_file.0003.png'],
        'image_file.0004.png': ['image_file.0005.png'],
        'image_file.0006.png': ['image_file.0007.png']}


if __name__ == "__main__":
    from tst_cfg import config as CONFIG

    # during normal pytest runs, config ("pkg_config") is picked up as a
    # fixture (from conftest.py)

    # ADD Tests for get_sequence_frange_data
    # test_get_sorted_sequence_filenames_finds_files(CONFIG)
    # test_get_sorted_sequence_filenames_reports_no_files(CONFIG)
    #
    # test_get_frame_numbers_finds_frame_numbers(CONFIG)

    ## this one parameterizes the following 5... can't run it from here
    # test_get_missing_frame_data()

    # test_get_missing_frame_data_returns_empty_list_when_all_frames_present(CONFIG)
    test_get_missing_frame_data_internal_1frame(CONFIG)
    # test_get_missing_frame_data_internal_isolated_2span_5span(CONFIG)
    # test_get_missing_frame_data_drop_first1_last1(CONFIG)
    # test_get_missing_frame_data_drop_first3_last3(CONFIG)

    # test_get_corrupt_frame_data_none(CONFIG)
    # test_get_corrupt_frame_data_first1_last1(CONFIG)
    # test_get_corrupt_frame_data_first3_last3(CONFIG)
    # test_get_corrupt_frame_data_internal_5span(CONFIG)
    ### VERIFY test_get_corrupt_frame_data_internal_isolated_2frames(CONFIG)
    # test_get_corrupt_frame_data_first_then_every_other(CONFIG)
    #test_get_corrupt_frame_data_all(CONFIG)
    #
    # test_fill_drop_frames_fills_missing_and_corrupt_frames()
    # test_get_existing_frange_returns_error_on_bad_framenumbers()
