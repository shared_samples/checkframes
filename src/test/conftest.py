"""
Any fixture created here is accessible to any test (@ or below THIS dir)
without the need to import anything

For install as part of "bron_unreal_api" as a package,
"<path-to>\bron_unreal_api\python"

is added to sys.path or PYTHONPATH in bron-launcher

so while getting tests running for DEMO. the BOOTSTRAP here
does the same

ULTIMATELY
'<path-to>\checkframes' should be used,
and all import-statements updated
"""
import os
import pathlib
from pytest import fixture
import re
import sys



from pprint import pprint
print("conftest.py")

# # BOOTSTRAP
# this_path = pathlib.Path(__file__)
# src_root = this_path.parents[1]
# sys.path.append(os.path.join(*src_root.parts))
# pprint(sys.path)

# PACKAGE IMPORTS
from environment import set_tool_runtime_environment
from tst_cfg import config as CONFIG
from domain import checkframes as cf
from domain import corrupt_frames as corr_frms
from domain import missing_frames as miss_frms
from domain import fix_frames as fix_frms

set_tool_runtime_environment()
@fixture(scope="session")
def pkg_config():
    return CONFIG

@fixture(scope="session")
def checkframes():
    return cf

@fixture(scope="session")
def corrupt_frames():
    return corr_frms

@fixture(scope="session")
def missing_frames():
    return miss_frms

@fixture(scope="session")
def replace_frames():
    return fix_frms.replace_frames

@fixture(scope="session")
def expected_frange():
    return ["1001", "1010"]


@fixture(scope="session")
def image_frame_regex():
    return re.compile(CONFIG["img_seq_filename_pattern"])


@fixture(scope="session")
def tst_resources():
    retval = os.environ["CFS_TEST_RESOURCES"]
    return retval
