# pylint: disable=E0401  # import-error
"""

"""
# STANDARD LIB
import os
from pprint import pprint

# THIRD PARTY LIB
from yaml import dump
from yaml import load
from yaml import warnings

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

try:
    from collections import ChainMap
except ImportError:
    # otherwise fall back on the 3rd party library
    # TODO get this installed to Bron py2 virtual-env
    # from chainmap import ChainMap
    pass

warnings({"YAMLLoadWarning": False})


def _get_config_raw_data(cfg_file_path):
    with open(cfg_file_path) as f:
        data = f.read()
    return load(data, Loader=Loader)


def get_config(cfg_file_path, runtime_environment_project=None):
    """

    Args:
        cfg_file_path (string): absolute path of cfg-file
        runtime_environment_project (string): repo-project, e.g. "dev_fables"

    Returns:
        config_rollup (ChainMap): containing either a project-dict or both
        a dev_project-dict and a project dict
        e.g. (fables-dict), or (dev_fables-dict, fables-dict)

    """
    if not runtime_environment_project:
        try:
            runtime_environment_project = os.environ["PROJECT"]
        except KeyError:
            runtime_environment_project = "dev_AAA"

    raw_config = _get_config_raw_data(cfg_file_path)

    # GET all project configs
    project_configs = raw_config.pop("project")
    """{'view_config': {'stylesheet_name': 'gui_dark.css'}, 
    'common': {
        'config': 'checkframes', 
        'img_seq_filename_pattern': '\\w+\\.(\\d{4})\\.([exrpng]{3})', 
        'image_seq_file_type': 'png', 
        'frange': ['1001', ''], 
        'size_delta_threshold': 80, 
        'min_size_bytes': 204800, 
        'fill_missing_frames': True, 
        'replace_corrupt_frames': False, 
        'dev_fill_missing_frames': False, 
        'fixed_img_seq_dirname': 'checkframe_fixed_img_seq', 
    'project': {'dev_AAA': {'project_token': 'AAA', 'phase3_renders_directory': 'C:\\Temp\\bron_digital\\src\\dcc\\unreal\\bron_unreal_api\\etc\\checkframes_resource\\tst_cases', 'dev_fill_missing_frames': True, 'dev_test_cmd_line_args': {'frange': ['1001', '1010'], 'gui': False}}, 'AAA': {'project_token': 'AAA', 'phase3_renders_directory': 'U:\\fables', 'fill_missing_frames': True}, 'BBB': {'project_token': 'BBB', 'phase3_renders_directory': 'U:\\gossamer'}}}, 'test': None}"""
    config_rollup = ChainMap()
    # Chainmap KEEPS FIRST key-value found.  "new_child()" pushes
    # dicts into the FIRST POSITION in the Chainmap... so we want the
    # to push in order of LOW-to-HIGH priority... last dict pushed RULES

    # GET "project" keys...
    if "dev_" in runtime_environment_project:
        proj_env_key = runtime_environment_project.split("dev_")[-1]
        dev_proj_env_key = runtime_environment_project
        runtime_proj_data_keys = [proj_env_key, dev_proj_env_key]
    else:
        proj_env_key = runtime_environment_project
        runtime_proj_data_keys = [proj_env_key]

    # ROLLUP project-configs
    for proj_key in runtime_proj_data_keys:
        dict_ = project_configs[proj_key]
        config_rollup = config_rollup.new_child(dict_)

    # Add the other dictionaries from config file
    config_rollup = config_rollup.new_child(raw_config["test"])
    config_rollup = config_rollup.new_child(raw_config["common"])
    config_rollup = config_rollup.new_child(raw_config["view_config"])

    return config_rollup


if __name__ == '__main__':
    runtime_project = os.environ["PROJECT"]
    # create a small test version of the config
    cfg_file_path_ = r"C:\Temp\bron_digital\src\dcc\unreal\bron_unreal_api\etc\shot_turnover_ingest_resource\shot_turnover_ingest_configuration.yml"
    print(get_config(cfg_file_path_, runtime_project))
