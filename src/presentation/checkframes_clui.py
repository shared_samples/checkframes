#!/usr/local/bin/python
# """Command-Line Interface Handler

# STANDARD IMPORTS
import argparse
import sys


def get_command_line_arguments():
    """
    Parse and return the command line arguments
    # TODO augment cmd-line arguements to include parameters that have defaults
       in config (like size_delta_threshold)
    """
    d_help = "specify image-sequence directory  \n"
    d_help += r'e.g. "U:\dropbox\fables\P3_renders\00_FB101_901_0010_MASTER"'
    f_help = "frame-range\n"
    f_help += r'; e.g. "-f 0 1000", '
    f_help += r'"-f 0000 0100"'
    t_help = "specify the adjacent frame size-change threshold. "
    t_help += "if the %%-change in size between adjacent frames is greater than "
    # TWO "%"... breaks with just one
    t_help += 'this threshold, then "flag" frame as possibly incomplete'

    m_help = r"Fill in MISSING Frames with an adjacent frame"
    c_help = r"Replace CORRUPT Frames with an adjacent frame"
    g_help = r"Run with a GUI"

    usage = "checkframes [-h] [-d DIRECTORY] "
    usage += "[-f START_FRAME END_FRAME] [-t] [-p] [-g]"
    description = "Creates image files for dropped frames in the image sequence at the given directory"
    parser = argparse.ArgumentParser(prog="checkframes", description=description)

    # want to be able to run
    #
    #      >filldropdata -g
    #
    # to launch GUI.... so CANNOT have "positional" command-line arguments
    # parser.add_argument("directory", action="store", help=d_help, type=str, default="")
    parser.add_argument(
        "-d", "--directory", action="store", help=d_help, type=str, default=""
    )
    parser.add_argument("-f", "--frange", action="store", nargs=2, help=f_help)
    parser.add_argument("-t", "--size_delta_threshold", action="store", help=t_help)
    parser.add_argument(
        "-m", "--fill_missing", action="store_true", default=False, help=m_help
    )
    parser.add_argument(
        "-c", "--replace_corrupt", action="store_true", default=False, help=c_help
    )
    parser.add_argument("-g", "--gui", action="store_true", default=False, help=g_help)

    cmd_line_args = parser.parse_args()

    if (not cmd_line_args.directory) and (not cmd_line_args.gui):
        print(usage)
        sys.exit()

    # convert from Namespace to dict (simplifies dev of both clui and gui (gui returns a dict..
    # I could've converted the dict to a Namespace before the gui returns its.... but it
    # here instead... random-choice)
    args = vars(cmd_line_args)

    return args


def report_results(results_report):
    """

    Args:
        results_report:

    Returns:

    """
    print("reporting from checkframes_clui")
    print("\n")
    print(results_report)
