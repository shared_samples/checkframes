# pylint: disable=E0401  # import-error
"""
GUI Presentation for Fill-Dropped-Frames

Implemented using the Model-View-Presenter pattern with Passive-View
"""
from __future__ import print_function

# STANDARD IMPORT
import os
import re
import sys
from pprint import pprint

# THIRD PARTY IMPORT
from qtpy.QtCore import Qt
from qtpy.QtGui import QColor
from qtpy.QtGui import QIcon
from qtpy.QtGui import QPalette
from qtpy.QtWidgets import QCheckBox

from qtpy.QtWidgets import QGridLayout
from qtpy.QtWidgets import QHBoxLayout
from qtpy.QtWidgets import QSpinBox
from qtpy.QtWidgets import QLabel
from qtpy.QtWidgets import QLineEdit
from qtpy.QtWidgets import QMessageBox
from qtpy.QtWidgets import QMainWindow

from qtpy.QtWidgets import QPushButton
from qtpy.QtWidgets import QVBoxLayout
from qtpy.QtWidgets import QWidget
from qtpy.QtWidgets import QFileDialog

WINDOW_FLAGS = (
    Qt.Window
    | Qt.WindowStaysOnTopHint
    | Qt.CustomizeWindowHint
    | Qt.WindowCloseButtonHint
    | Qt.WindowTitleHint
    | Qt.WindowMaximizeButtonHint
)


class CheckFramesView(QMainWindow):
    """
    A PASSIVE-VIEW; NO LOGIC
    """

    def __init__(self, parent=None, config=None):
        try:
            super().__init__(parent, WINDOW_FLAGS)
        except TypeError as msg:
            print("Failure initialising Retrieval --> self: {msg}")
            raise

        # Window Settings
        self.WINDOW_TITLE = "     CHECKFRAMES"
        self.WINDOW_VERSION = 1.0

        # self.width used elsewhere to position Message-Box
        # relative to Main-window
        self.width = 450
        self.resize(self.width, 150)
        self.setAttribute(Qt.WA_DeleteOnClose)
        """Ensures this is removed from MEMORY on close"""

        self._browse_btn_icon_path = os.path.join(
            os.environ["CFS_PACKAGE_ICONS"], "folder-yellow.ico"
        )

        self._config = config
        self._create_widget()

    def _create_widget(self):
        """Create, Setup and Layout widgets"""
        self._create_widgets()
        self._configure_widgets()
        self._layout_widgets()

    def _create_widgets(self):
        self.container = QWidget(self)

        self._lbl_directory = QLabel()
        self.lnEdt_directory = QLineEdit()
        self.btn_browse = QPushButton()

        self.lbl_frame_range = QLabel()
        self.lnEdt_start_frame = QLineEdit()
        self.lnEdt_end_frame = QLineEdit()

        self.chkBx_fill_missing = QCheckBox()
        self.chkBx_fill_corrupt = QCheckBox()

        self._lbl_threshold = QLabel()
        self.spnBx_threshold = QSpinBox()

        self.btn_check = QPushButton()
        self.btn_quit = QPushButton()

        self.qFlDlg_directory_selector = QFileDialog(self, Qt.WindowStaysOnTopHint)

    def _configure_widgets(self):
        title = f"{self.WINDOW_TITLE}-{str(self.WINDOW_VERSION)}"
        self.setWindowTitle(title)
        # self.setWindowIcon(QIcon(r"C:\Temp\bron_digital\src\core\act\etc\icons\folder-yellow.ico")

        self._lbl_directory.setText("Image Sequence Directory")

        self.lnEdt_directory.setTextMargins(5, 5, 5, 5)
        self.lnEdt_directory.setText("Browse to select directory")
        self.btn_browse.setIcon(QIcon(self._browse_btn_icon_path))

        self.lbl_frame_range.setText("Frame Range:")

        self.lnEdt_start_frame.setText(self._config["frange"][0])
        self.lnEdt_end_frame.setText(self._config["frange"][1])

        self.chkBx_fill_missing.setText("Add Placeholders For Missing Frames")
        self.chkBx_fill_missing.setChecked(self._config["fill_missing_frames"])
        self.chkBx_fill_corrupt.setText("Add Placeholders For Incomplete Frames")
        self.chkBx_fill_corrupt.setChecked(self._config["replace_corrupt_frames"])

        self._lbl_threshold.setText("Unfinished Frame Size delta:")
        self.spnBx_threshold.setRange(10, 90)
        self.spnBx_threshold.setSingleStep(5)
        self.spnBx_threshold.setValue(self._config["size_delta_threshold"])
        self.spnBx_threshold.setSuffix("%")

        # TODO enable only when valid data
        #  easy to just do once directory is populated but
        #  better if also checks end_frame is populated

        self.btn_check.setEnabled(False)
        self.btn_check.setText("CHECK Frames")
        self.btn_quit.setText("Cancel")

        self.qFlDlg_directory_selector.setModal(True)
        self.qFlDlg_directory_selector.setSizeGripEnabled(True)
        self.qFlDlg_directory_selector.setFileMode(QFileDialog.Directory)
        self.qFlDlg_directory_selector.setViewMode(QFileDialog.Detail)
        start_dir = self._config["phase3_renders_directory"]
        if os.path.isdir(start_dir):
            self.qFlDlg_directory_selector.setDirectory(start_dir)

    def _layout_widgets(self):
        """Create Layout-widgets and populate with the other widgets

        +-----------------------------------------------------+
        |    Check Frames                                     |
        +-----------------------------------------------------+
        |                           +-------------+  +------+ |
        |  Image Sequence Directory:|             |  |Browse| |
        |                           +-------------+  +------+ |
        |                        +-----------+  +----------+  |
        |  EXPECTED Frame Range: |           |  |          |  |
        |                        +-----------+  +----------+  |
        |   +---+                                             |
        |   |   |  Create Placeholder Frames                  |
        |   +---+                                             |
        |                               +------+              |
        |  Unfinished Frame Size Delta: |      |              |
        |                               +------+              |
        |  +----------------------------------+  +---------+  |
        |  |          CHECK Frames            |  |  Cancel |  |
        |  +----------------------------------+  +---------+  |
        +-----------------------------------------------------+
        """
        # - layouts
        lyt_main = QVBoxLayout(self.container)

        lyt_directory = QHBoxLayout()

        lyt_frange_container = QHBoxLayout()
        lyt_placeholder = QVBoxLayout()
        lyt_threshold = QHBoxLayout()
        lyt_buttons = QGridLayout()

        #    add widgets to layouts
        lyt_directory.addWidget(self._lbl_directory)
        lyt_directory.addWidget(self.lnEdt_directory)
        lyt_directory.addWidget(self.btn_browse)

        lyt_frange_container.addWidget(self.lbl_frame_range)
        lyt_frange_container.addWidget(self.lnEdt_start_frame)
        lyt_frange_container.addWidget(self.lnEdt_end_frame)
        lyt_frange_container.addSpacing(200)

        lyt_placeholder.addWidget(self.chkBx_fill_missing)
        lyt_placeholder.addWidget(self.chkBx_fill_corrupt)

        lyt_threshold.addWidget(self._lbl_threshold)
        lyt_threshold.addWidget(self.spnBx_threshold)
        lyt_threshold.addSpacing(200)
        #    add layouts to layouts
        lyt_main.addLayout(lyt_directory)
        lyt_main.addLayout(lyt_frange_container)
        lyt_main.addLayout(lyt_placeholder)
        lyt_main.addLayout(lyt_threshold)
        lyt_main.addLayout(lyt_buttons)

        # If the QGridLayout is not the top-level layout (i.e. does not manage
        # all of the widgets area and children), you must add it to its parent
        # layout when you create it, but before you do anything with it.
        # The normal way to add a layout is by calling addLayout() on the
        # parent layout.
        lyt_buttons.addWidget(self.btn_check, 0, 0)
        lyt_buttons.addWidget(self.btn_quit, 0, 1)
        lyt_buttons.setColumnStretch(0, 1)

        self.setCentralWidget(self.container)


class CheckFramesInteractor(object):
    """Interactor contains GUI logic"""

    def __init__(self, presentation, view):
        self.view = view
        self._presentation = presentation

    def connect_to_view(self):
        """
        Sets up CONNECTIONS between VIEW widgets and PRESENTATION methods
        """

        # skip connecting when testing
        if self.view:
            self.view.btn_browse.clicked.connect(self.browse_for_directory_clicked)
            self.view.btn_check.clicked.connect(self.checkframes_clicked)
            self.view.btn_quit.clicked.connect(self.cancel_clicked)

        else:
            print("Warning: No VIEW object present; OK during tests")

    # CALLBACK METHODS
    def browse_for_directory_clicked(self):
        """
        A "loop-back" handler; i.e. doesn't call PRESENTATION methods.
        - handles signal from VIEW, by  affecting VIEW

        """
        if self.view.qFlDlg_directory_selector.exec():
            directory = self.view.qFlDlg_directory_selector.selectedFiles()[0]

            self.view.lnEdt_directory.clear()
            self.view.lnEdt_directory.insert(directory)
            self.view.btn_check.setEnabled(True)
            self._presentation.handle_browse_for_directory_clicked(directory)

    # ACTIONS/CALLBACKS
    def checkframes_clicked(self, test_model=None):
        """

        Returns:

        """
        if self.view:
            model = {}

            model["directory"] = self.view.lnEdt_directory.text()
            model["frange"] = [
                self.view.lnEdt_start_frame.text(),
                self.view.lnEdt_end_frame.text(),
            ]
            model["size_delta_threshold"] = self.view.spnBx_threshold.value()
            model["fill_missing"] = self.view.chkBx_fill_missing.isChecked()
            model["replace_corrupt"] = self.view.chkBx_fill_corrupt.isChecked()
        else:
            model = test_model

        self._presentation.handle_checkframes_clicked(model)

    def cb_browse_for_directory_clicked(self, existing_frange):
        """Once obtained existing frame range of image-sequence under
        "directory", 
        1. update GUO with the discovered frame range
        2. notify user to verify/adjust the expected frame
        range (if its necessary)
        """
        start = str(existing_frange[0])
        end = str(existing_frange[1])

        self.view.lnEdt_start_frame.setText(start)
        self.view.lnEdt_end_frame.setText(end)

        msg = f"NOTE: Detected Frame Range = {start}-{end}.\n\n"
        msg += 'If necessary, update "Frame Range" to the EXPECTED Frame Range.  '
        msg += '(This allows reporting of any problem frames at Start/End of Frame Range)'
        
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Information)
        msg_box.setText(msg)
        
        # Position QMessageBox so user can see the "Frame Range" widget, referred
        # to in the message.
        msg_box.move(self.view.pos().x() + self.view.width, self.view.pos().y())
        # Highlight the "Frame Range" widget TEXT to further guide the user
        self.view.lbl_frame_range.setStyleSheet("color: red;")
        msg_box.exec()
        # Reset the "Frame Range" widget TEXT, once the QMessageBox is cleared
        self.view.lbl_frame_range.setStyleSheet("")

    def cb_checkframes_clicked(self, reply):
        """
        for reporting results back to UI
        reply = {"report_data": report_data,
                 "report_string": report_string}

        """
        print(reply["report_string"])

    def cancel_clicked(self):
        """

        Returns:

        """
        self._presentation.handle_cancel_clicked()


class CheckFramesPresenter(CheckFramesInteractor):
    """ """

    def __init__(self, view=None, request_handlers=None, config=None):
        super(CheckFramesPresenter, self).__init__(self, view)
        self.request_handlers = request_handlers
        self.config = config
        self.connect_to_view()

    # INTERACTOR SIGNAL-HANDLER CALLBACKS
    def handle_browse_for_directory_clicked(self, directory):
        """

        Args:
            directory:

        Returns:

        """
        get_frange_data = self.request_handlers["get_sequence_frange_data"]

        frange_data = get_frange_data(directory, self.config)
        if frange_data["errors"]:
            # TODO report error (pop-up?)... contact pipeline...etc
            pass
        else:
            existing_frange = frange_data["existing_frange"]

            self.cb_browse_for_directory_clicked(existing_frange)

    def handle_checkframes_clicked(self, model):
        """

        Args:
            models:

        Returns:

        """
        print("{directory}, {frange}".format(**model))

        self.request_handlers["checkframes_controller"](
            self.config, **model, reply_method=self.cb_checkframes_clicked
        )

    @staticmethod
    def handle_cancel_clicked():
        """

        Returns:

        """
        sys.exit(0)

    # ACTIONS


def create_and_show_checkframes_gui(config, domain_api):
    from qtpy.QtWidgets import QApplication

    app = QApplication([])
    # QApplication instance MUST be created before ANY other widgets

    stylesheet_name = config["stylesheet_name"]
    stylesheet_path = os.path.join(os.environ["CFS_PACKAGE_RESOURCES"], stylesheet_name)

    stylesheet = ""
    with open(stylesheet_path) as fh:
        stylesheet = fh.read()
    app.setStyleSheet(stylesheet)

    view = CheckFramesView(config=config)
    pres = CheckFramesPresenter(view=view, request_handlers=domain_api, config=config)

    view.show()
    app.exec_()
    # Starts event loop


def run_checkframes_gui(config=None, domain_api=None):
    if domain_api:
        create_and_show_checkframes_gui(config, domain_api)
