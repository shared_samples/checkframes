# pylint: disable=E0401  # import-error
"""
filesystem service

TemporaryDirectory - notes on deletion
    "On completion of the context or destruction of the temporary directory object,
    the newly created temporary directory and all its contents are removed
    from the filesystem.

    "The directory can be explicitly cleaned up by calling the cleanup() method.

NamedTemporaryFile - notes
   Has the "delete" arguement, missing from "TemporaryFile.
   "If delete is true (the default), the file is deleted as soon as it is closed.
"""
# STANDARD
import logging
import os
import shutil

from tempfile import mkdtemp, TemporaryDirectory

# THIRD PARTY LIB
from yaml import dump
from yaml import load

# from yaml import warnings

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

logging.basicConfig(level=logging.DEBUG)
# LOGGER = logging.getLogger("check_frames.{}".format(__package__))
LOG = logging.getLogger(__package__)
LOG.setLevel(logging.INFO)

def get_self_destructing_temp_dir_obj(non_standard_tempdir_location=None):
    """
    Python 3.10 + only (for "ignore_cleanup_errors" parameter)

    On destruction of the TemporaryDirectory object,
    the newly created temporary directory and all its contents are
    removed from the filesystem.

    Args:
        dirname:
        non_standard_tempdir_location:

    Returns:
        dir_uri (string):

    """
    if non_standard_tempdir_location:
        temp_dir = TemporaryDirectory(dir=non_standard_tempdir_location,
                                      ignore_cleanup_errors=True)
    else:
        temp_dir = TemporaryDirectory(ignore_cleanup_errors=True)
    return temp_dir


def get_temp_dir_obj(non_standard_tempdir_location=None):
    """
    On destruction of the TemporaryDirectory object,
    the newly created temporary directory and all its contents are
    removed from the filesystem.

    The directory can be explicitly cleaned up by calling the cleanup() method

    Args:
        dirname:
        non_standard_tempdir_location:

    Returns:
        dir_uri (string):

    """
    if non_standard_tempdir_location:
        temp_dir = TemporaryDirectory(dir=non_standard_tempdir_location)
    else:
        temp_dir = TemporaryDirectory()
    return temp_dir


def save_data_to_tempfile(data, filename=None):
    """

    Args:
        data:
        filename:

    Returns:

    """
    if not filename:
        filename = "temp"
    file_uri = _get_data_file_uri(filename)
    serialized = dump(data, Dumper=Dumper)
    with open(file_uri, "w") as a_file:
        a_file.write(serialized)
    return file_uri


def copy_file(src_path, dst_path):
    """
    Copy source-paths to destination-paths

    Args:
        src_path (str):
        dst_path (str):

    Returns:

    """
    error = ""
    dst_location = os.path.dirname(dst_path)

    try:
        shutil.copy(src_path, dst_path)
    except IOError as msg:
        # print("IOError: {}".format(msg))
        LOG.debug("Creating Directories: {}".format(dst_location))
        try:
            os.makedirs(dst_location)
        except WindowsError as msg:
            pass
            # print("WindowsError: {}".format(msg))
            # msg is [Error 183] Cannot create a file when that file already exists:
        try:
            shutil.copy(src_path, dst_path)
        except OSError as msg:
            error = [msg]
        except IOError as msg:
            error = [msg]
        if error:
            error.extend([src_path, dst_path])
    return error



def copy_files(src_dst_paths):
    """
    Copy source-paths to destination-paths

    Args:
        src_dst_paths (dict): maps src-paths to dst-paths

    Returns:


    """
    error = ""
    for src, dst in src_dst_paths.items():
        copy_file(src, dst)

    return error

def retrieve(file_uri):
    """

    Args:
        file_uri:

    Returns:

    """

    if file_resource_ok(file_uri):
        with open(file_uri, "r") as a_file:
            serialized = a_file.read()
        return load(serialized, Loader=Loader)


def file_resource_ok(file_uri):
    """

    Args:
        file_uri (string):

    Returns:
        bool, True if file is valid (looks like a file) AND its not empty
    """
    status = False
    if os.path.isfile(file_uri) and os.path.getsize(file_uri) > 0:
        status = True
    return status


def _get_data_file_uri(filename):
    temp_dir = mkdtemp()
    return os.path.join(temp_dir, filename)


def _tst():
    print("tst")
    data_ = {"test": "TEST"}
    data_file_uri = save_data_to_tempfile(data_, "testfilename.yaml")
    assert retrieve(data_file_uri) == data_


if __name__ == "__main__":
    _tst()
