"""
... util used by report modules
"""
# STANDARD IMPORTS
import csv
import datetime
import os
import tempfile

from html import escape

# PACKAGE IMPORTS

try:
    import data.filesystem_service as filesystem
except ModuleNotFoundError:  # when debugging
    import src.data.filesystem_service as filesystem

def csv_2_html(columns, csv_uri, filename):
    """Reads csv data, creates html table data and writes to filesystem

    Args:
        columns:
        csv_uri:
        filename:

    Returns:
        report_tmp_uri (str): path of temp file
    """
    rows = read_csv(csv_uri, columns)

    # hack - remove extra column headers
    rows = rows[1:]
    # records = [r for r in rows]

    htm_table = html_table(rows)
    # print(f"htm_table:{htm_table}")

    # GET File PATH's
    temp_report_root_dir_obj = filesystem.get_temp_dir_obj()
    report_tmp_uri = os.path.join(tempfile.mkdtemp(), filename)

    with open(report_tmp_uri, "w", newline="") as f:
        for line in htm_table:
            f.write(line)
    return report_tmp_uri


def read_csv(path, column_names):
    """

    Args:
        path:
        column_names:

    Returns:
        rows (list of dicts)
    """
    rows = []
    with open(path, newline="") as f:
        # why newline="": see footnote at the end of https://docs.python.org/3/library/csv
        reader = csv.reader(f)
        for row in reader:
            record = dict(zip(column_names, row))
            rows.append(record)
    return rows


def html_table(records):
    """

    Args:
        records (list of dicts):

    Returns:
        string: Table of data in HTML


    We know the number of expected distict row-values
    (one SHOT col + two each of
    cut-in, cut-out, duration, tc_src_in, tc_src_out, turnover-location columns)

    if turnover-locations are the same ---- we don't get here, since no lineupsheet needs to be created
    so, a SET of the values will be LENGTH = 8 (1 for shot, 5 for timing, 2 for location)
    if LENGTH > 8, then there is some CHANGE in timing data -- SET THIS ROW TO BOLD

    # TODO - better description, less janky... this is prolly a note for the CLIENT of this
    """
    column_names = []
    # first detect all posible keys (column names) that are present in records
    for record in records:
        for name in record.keys():
            if name not in column_names:
                column_names.append(name)

    # create the HTML line by line
    lines = [STYLE]
    lines.append('<table border="2">\n  <tr>\n')
    for name in column_names:
        lines.append("    <th>{}</th>\n".format(escape(name)))
    lines.append("  </tr>\n")
    for record in records:
        lines.append("  <tr>\n")
        for name in column_names:
            value = record.get(name, "")
            lines.append("    <td>{}</td>\n".format(escape(value)))
        lines.append("  </tr>\n")
    lines.append("</table>")
    # join the lines to a single string and return it
    return "".join(lines)

def datetimestamp(precision="minute", no_century=True):
    """

    Args:
        precision (string): ["date" | "minute" | "hi"]
        no_century:

    Returns:
        datetimestamp (str):  e.g.
               precision = "hi",       220725142438396652
               precision = "minute",   2207251424
               precision = "date",     220725

               no_century=False AND
               precision = "date",     20220725
    """

    today = datetime.date.today()
    today = "".join(str(today).split("-"))

    if no_century:
        today = today[2:]  # remove first two digits from year

    if precision == "date":
        date_time_stamp = today
    else:
        # Get high-precision time
        now = datetime.datetime.now()
        time_obj = now.time()
        time = "".join(str(time_obj).split(":"))
        time = "".join(time.split("."))
        # if before 1000h, insert 0
        # if 0 dropped in time, then length will be 11 not 12
        if len(time) < 12:
            time = "0" + time

        date_time_stamp = today + time
        if precision == "minute":
            date_time_stamp = date_time_stamp[:10]
    return date_time_stamp

STYLE = """
<style type="text/css">
html
{
  font-family: "Courier New";

}
table
{
  border-collapse: collapse;
  border: 3px solid black;
}
th
{
  border: 3px solid black;
  font-size: 90%;
  #font-family: "Calibri";
  color:#555555;
  padding:0 15px 0 15px;
}
td
{
  padding:0 15px 0 15px;
  border: 1px solid black;

}
caption
{
  text-align: left;
}
</style>\n
"""
