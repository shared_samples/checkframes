"""

"""

# STANDARD IMPORTS
import csv
import logging
import os
import shutil
from copy import deepcopy

# PACKAGE IMPORTS
try:
    from domain.util.report_utils import csv_2_html
    from domain.util.report_utils import datetimestamp
except ImportError:  # when debugging
    from src.domain.util.report_utils import csv_2_html
    from src.domain.util.report_utils import datetimestamp


logging.basicConfig(level=logging.DEBUG)
# LOGGER = logging.getLogger("check_frames.{}".format(__package__))
LOG = logging.getLogger(__package__)
LOG.setLevel(logging.INFO)


def get_report(img_seq_dir, fill_missing, replace_corrupt, config):
    """Construct and return Reporter instance"""
    return Report(img_seq_dir, fill_missing, replace_corrupt, config)


class Report():
    """

    Args:
        img_seq_dir:
        fill_missing:
        replace_corrupt:
        config:
    """

    def __init__(self, img_seq_dir, fill_missing, replace_corrupt, config):

        self._img_seq_dir = img_seq_dir
        self._fill_missing = fill_missing
        self._replace_corrupt = replace_corrupt
        self._config = config

        self._csv_filename = self._get_report_filename(file_type="csv")
        self._htm_filename = self._get_report_filename(file_type="html")

        self._csv_rprt_tmp_uri = ""
        self._htm_rprt_tmp_uri = ""
        self._report_data = {
            "errors": [],
            "warnings": [],
            "fill_missing_frames_report": {
                "errors": [],
                "fill_filled_frames": {},
                "warnings": [],
            },
            "replace_corrupt_frames_report": {
                "errors": [],
                "fill_filled_frames": {},
                "warnings": [],
            },
            "min_max_frame_size": [],
            "min_max_percent_size_delta": [],
            "corrupt_file_data": [],
        }

    def __del__(self):
        try:
            LOG.debug(f"Report cleanup {self._temp_root_dir_obj.name}")
            self._temp_root_dir_obj.cleanup()
        except Exception:  # pylint: disable= W0703  # (broad-except)
            # pre Python3.10, TemporaryDirectory object doesn't have the
            # "ignore_cleanup_errors" parameter.... so sorta doing that
            # manually here
            pass

    @property
    def errors(self):
        """
        For collecting internal errors, and exposing to clients
        Returns:
            self._errors (list):error message-strings accumulated
                                during execution of methods
        """
        errors = self.flatten(self._errors)
        return list(set(errors))

    @errors.setter
    def errors(self, value):
        self._errors.append(value)

    def flatten(self, list_of_lists):
        """flattens multi-level list of lists

        used to handle the lists of error strings that accumulate
        several modules
        """
        if len(list_of_lists) == 0:
            return list_of_lists
        if isinstance(list_of_lists[0], list):
            return self.flatten(list_of_lists[0]) + self.flatten(list_of_lists[1:])
        return list_of_lists[:1] + self.flatten(list_of_lists[1:])

    def execute(self, reports_dst_dir):
        """do it"""
        self._create_reports()
        csv_final_uri, htm_final_uri = self._save_reports(reports_dst_dir)
        return csv_final_uri, htm_final_uri

    def _create_reports(self):
        self._csv_rprt_tmp_uri, column_names = self._create_csv_report()
        self._htm_rprt_tmp_uri = csv_2_html(
            column_names,
            self._csv_rprt_tmp_uri,
            self._csv_filename,
        )

    def _save_reports(self, dst_dir):
        csv_final_uri = os.path.join(dst_dir, self._csv_filename)
        htm_final_uri = os.path.join(dst_dir, self._htm_filename)

        # Cleanup existing report files
        #   existing reports will be there if redo an ingest on same TO folder
        if os.path.isfile(csv_final_uri):
            try:
                os.remove(csv_final_uri)
            except PermissionError as msg:
                LOG.debug(msg)
                # file in use... tag uri so we can still write data to file
                pre_ext, ext = csv_final_uri.split(".")
                tagged_pre_ext = "_".join([pre_ext, date_tag()])
                csv_final_uri = ".".join([tagged_pre_ext, ext])

        if os.path.isfile(htm_final_uri):
            try:
                os.remove(htm_final_uri)
            except PermissionError as msg:
                LOG.debug(msg)
                # file in use... tag uri so we can still write data to file
                pre_ext, ext = htm_final_uri.split(".")
                tagged_pre_ext = "_".join([pre_ext, utils.datetimestamp()])
                htm_final_uri = ".".join([tagged_pre_ext, ext])

        for src_uri, dst_uri in [
            (self._csv_rprt_tmp_uri, csv_final_uri),
            (self._htm_rprt_tmp_uri, htm_final_uri),
        ]:
            if os.path.isfile(src_uri):
                try:
                    shutil.copy(src_uri, dst_uri)
                    LOG.info(f"\tSaving EDL Report: {dst_uri}")

                except Exception as msg:
                    err_msg = "Error copying EDL report to "
                    err_msg += f"turnover-directory...\n {dst_uri}"
                    LOG.error(msg, err_msg)
                    self.errors = "\n".join([msg, err_msg])

            else:
                err_msg = f"Error writing EDL report to local disk...\n {src_uri}"
                LOG.error(err_msg)
                self.errors = err_msg

        return csv_final_uri, htm_final_uri

    def _create_csv_report(self):
        """
         One ROW per VFX SHOT
         create dict {shot_name: {shot_row}}

        "edl_report_rows[shot_name]" keys are the column names; are the
        user's terms (user-domain naming)


         Args:
             turnover:

         Returns:
             edl_csv_report_tmp_uri (string):

        """
        report_rows = {}

        for shot_name, event_data in self._turnover_data.edl_shot_ingest_data.items():
            report_rows[shot_name] = []
            event_row = deepcopy(EVENT_ROW_TEMPLATE)

            if len(report_rows[shot_name]) == 0:
                event_row["Shot Name"] = shot_name
            else:
                event_row["Shot Name"] = ""
            event_row["Edit Event"] = event_data["id"]
            event_row["Source-Clip Name(s)"] = ", ".join(
                [event_data["from_clip"], event_data["to_clip"]]
            )
            event_row["Duration\n[Frames]"] = event_data["frm_edt_duration"]
            event_row["Source Timecode In"] = event_data["tc_src_in"]
            event_row["Source Timecode Out"] = event_data["tc_src_out"]
            event_row["Edit Timecode In"] = event_data["tc_edt_in"]
            event_row["Edit Timecode Out"] = event_data["tc_edt_out"]
            event_row["Editorial Note"] = event_data["notes"]

            report_rows[shot_name].append(event_row)

        # GET File PATH's
        report_filename = "edl_report.csv"  # get from config
        report_tmp_uri = os.path.join(self._temp_root_dir_obj.name, report_filename)

        # GET data for CSV-Writer
        row_data = []
        csv_columns = None
        for _, rows_list in report_rows.items():
            for row in rows_list:
                row_data.append(row)
                if not csv_columns:
                    csv_columns = list(row.keys())

        with open(report_tmp_uri, "w", newline="") as fhandle:
            writer = csv.DictWriter(fhandle, fieldnames=csv_columns)
            writer.writeheader()
            for data in row_data:
                writer.writerow(data)

        if os.path.isfile(report_tmp_uri) and os.stat(report_tmp_uri).st_size > 0:
            result = report_tmp_uri
        else:
            result = None
        return result, csv_columns

    def _get_report_base_filename(self):
        prj_token = self._config["project_token"]

        date_token = datetimestamp(precision="minute")

        return "_".join([prj_token, "checkframes", date_token])

    def _get_report_filename(self, file_type):
        return ".".join([self._get_report_base_filename(), file_type])
