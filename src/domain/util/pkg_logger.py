"""
common setup for the tool"s modules
"""
# STANDARD
import logging
import sys


def get_pkg_logger(module_hierachical_name):
    """

    Args:
        module_hierachical_name (string): __name__

    Returns:
        log (object): handle to new logger
    """
    logger_name = module_hierachical_name.split(".")[-1]
    log = logging.getLogger(logger_name)
    if log.hasHandlers():
        log.handlers.pop()
    strm_handler = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    formatter = logging.Formatter(" %(levelname)s - %(message)s")
    strm_handler.setFormatter(formatter)
    log.addHandler(strm_handler)
    # just the default level -
    log.setLevel(logging.INFO)
    return log
