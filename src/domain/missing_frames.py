"""missing_frames.py
"""

# STANDARD IMPORTS
import re
# PACKAGE IMPORTS
try:  # when testing
    import data.filesystem_service as filesystem
except ModuleNotFoundError:  # when deployed
    import src.data.filesystem_service as filesystem


def get_missing_frame_data(
    img_seq_filenames,
    expected_frange,
    config,
):
    """
    Creates a data-structure (frame_fill_data) for passing to "replace_frames"

    Args:
        img_seq_filenames (sorted list):
        expected_frange (list): [start-frame, end-frame]
        condigf:

     Returns:
         frame_fill_data (dict):
             {"frame_fill_spec":[<data-tuple>, ...],
             "filler_filename_template":
              "warnings":[<string>, ...]

         where,
             <data-tuple> = (<number-of-dropped-frames>,
                             <frame-number-of-adjacent-valid-frame>)
               e.g. [(2, '0036'), (-3, '0089')]
               ==> The sequence has dropped frames 0037, 0038, 0086, 0087, 0088

             filler_filename_template is used by "replace_frames" to construct filler filenames
    """
    missing_frame_data = {
        "missing_file_data": {},
        "frame_fill_data": {},
    }
    frame_fill_data = {"frame_fill_spec": [], "filler_filename_template": []}

    existing_frame_numbers = _get_existing_frame_numbers(
        img_seq_filenames, config
    )

    # GET inner_missing_frames ("inner" == not at beginning or end)
    inner_frame_fill_spec = _get_inner_frame_fill_spec(existing_frame_numbers)
    # inner_missing_frames = _get_inner_frame_fill_spec__(img_seq_filenames, config)
    if inner_frame_fill_spec:
        frame_fill_data["frame_fill_spec"].extend(inner_frame_fill_spec)
    # GET outer_missing_frames (at beginning or end of sequence)

    # outer_missing_frames = _get_outer_frame_fill_spec_old(
    #     expected_frange, first_existing_frame, last_existing_frame
    # )
    outer_frame_fill_spec = _get_outer_frame_fill_spec(
        expected_frange, existing_frame_numbers
    )
    if outer_frame_fill_spec:
        frame_fill_data["frame_fill_spec"].extend(outer_frame_fill_spec)

    filler_filename_template = img_seq_filenames[0].split(".")
    frame_fill_data["filler_filename_template"] = filler_filename_template

    missing_frame_data["frame_fill_data"] = frame_fill_data

    missing_file_data = {}
    for frame_fill_spec in frame_fill_data["frame_fill_spec"]:
        frm_span = frame_fill_spec[0]
        frm_num = frame_fill_spec[1]

        frm_increment = 1
        if frm_span < 0:
            frm_increment = -1
        increment = frm_increment
        for i in range(abs(frm_span)):
            fill_frm_num = str(int(frm_num) + increment).zfill(4)
            increment += frm_increment
            missing_filename = f"{filler_filename_template[0]}.{fill_frm_num}.{filler_filename_template[2]}"
            missing_file_data[missing_filename] = fill_frm_num
    missing_frame_data["missing_file_data"] = missing_file_data
    """
    {'missing_file_data': {
        'image_file.1003.png': '1003', 
        'image_file.1005.png': '1005', 
        'image_file.1007.png': '1007', 
        'image_file.1009.png': '1009', 
        'image_file.1001.png': '1001'}, 
    'frame_fill_data': {
        'frame_fill_spec': [(1, '1002'), (1, '1004'), (1, '1006'), (1, '1008'), (-1, '1002')], 
        'filler_filename_template': ['image_file', '1002', 'png']}}"""
    return missing_frame_data


def _get_existing_frame_numbers(filenames, config):
    """

    Args:
        filenames (list):
        image_frame_regex:

    Returns:
        sorted list of frame-numbers
    """
    image_frame_regex =  re.compile(config["img_seq_filename_pattern"])
    frame_numbers = []
    for filename in filenames:
        match = image_frame_regex.match(filename)
        frame_numbers.append(match.group(1))

    return sorted(frame_numbers)


def _get_inner_frame_fill_spec__(img_seq_filenames, config):
    """

    Args:
        sorted_framenumbers:

    Returns:
        inner_missing_frames (list): framenumbers of frames that are missing
            but NOT from the beginning or end of the sequence
    """
    existing_frame_numbers = _get_existing_frame_numbers(
        img_seq_filenames, config
    )

    inner_missing_frames = []
    index = 0
    while len(existing_frame_numbers) > index + 1:
        n_1 = int(existing_frame_numbers[index])
        n_2 = int(existing_frame_numbers[index + 1])

        span = n_2 - n_1
        if span == 1:
            pass
        else:
            pre_missing_frame = str(existing_frame_numbers[index]).zfill(4)
            inner_missing_frames.append((span - 1, pre_missing_frame))
        index += 1

    return inner_missing_frames

def _get_inner_frame_fill_spec(existing_frame_numbers):
    """

    Args:
        existing_frame_numbers:

    Returns:
        inner_frame_fill_spec: (list of tuples): tuple = (<span>, <framenumber of filler frame>)

           e.g. if frames 1003-1005 are missing,
               inner_frame_fill_spec = [(3, 1002)]

           --> specifies to copy the frame "1002" img-file
               to create img-files for frames 1003, 1004, 1005,
    """
    inner_missing_frames = []
    index = 0
    while len(existing_frame_numbers) > index + 1:
        n_1 = int(existing_frame_numbers[index])
        n_2 = int(existing_frame_numbers[index + 1])

        span = n_2 - n_1
        if span == 1:
            pass
        else:
            pre_missing_frame = str(existing_frame_numbers[index]).zfill(4)
            inner_missing_frames.append((span - 1, pre_missing_frame))
        index += 1

    return inner_missing_frames


def _get_outer_frame_fill_spec(expected_frange, existing_frame_numbers):
    """

    Args:
        expected_frange:
        existing_frame_numbers:


    Returns:
        outer_frame_fill_spec: (list of tuples): tuple = (<span>, <framenumber of filler frame>)

           e.g. if 1st frame missing,
           outer_frame_fill_spec = [(-1, 1002)]

           --> specifies to copy the frame "1002" img-file
               to create img-file for frame 1001,
    """
    outer_missing_frames = []

    first_existing_frame = int(existing_frame_numbers[0])
    last_existing_frame = int(existing_frame_numbers[-1])

    expected_first_frame = int(expected_frange[0])
    expected_last_frame = int(expected_frange[-1])

    if last_existing_frame < expected_last_frame:
        span = expected_last_frame - last_existing_frame
        pre_missing_frame = str(last_existing_frame).zfill(4)
        outer_missing_frames.append((span, pre_missing_frame))

    if first_existing_frame > expected_first_frame:
        span = first_existing_frame - expected_first_frame
        post_missing_frame = str(first_existing_frame).zfill(4)
        outer_missing_frames.append((-span, post_missing_frame))
    return outer_missing_frames

def _get_outer_frame_fill_spec_old(
    expected_frange, first_existing_frame, last_existing_frame
):
    """

    Args:
        expected_frange:
        first_existing_frame:
        last_existing_frame:

    Returns:
        outer_missing_frames: (list): framenumbers of frames that are missing
            from the beginning or end of the sequence
    """
    outer_missing_frames = []
    expected_first_frame = int(expected_frange[0])
    expected_last_frame = int(expected_frange[-1])

    if last_existing_frame < expected_last_frame:
        span = expected_last_frame - last_existing_frame
        pre_missing_frame = str(last_existing_frame).zfill(4)
        outer_missing_frames.append((span, pre_missing_frame))

    if first_existing_frame > expected_first_frame:
        span = first_existing_frame - expected_first_frame
        post_missing_frame = str(first_existing_frame).zfill(4)
        outer_missing_frames.append((-span, post_missing_frame))
    return outer_missing_frames
