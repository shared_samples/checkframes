"""corrupt_frames.py


"""

import os
import re
from collections import OrderedDict
from copy import deepcopy
from pprint import pprint  # pylint: disable=W0611  # unused-import


def get_corrupt_frame_data(src_dir, image_seq_filenames, config):
    """

    Args:
        src_dir (string): absolute path of image sequence directory
        image_seq_filenames (list): filenames sorted by frame number

    Returns:
        corrupt_frame_data (dict):

        {'corrupt_file_data': {'<file-name>: <file-size>,
                                    ...},
         'frame_fill_data': {'filler_filename_template': ['image_file', '1004', 'png'],
                             'frame_fill_spec': []},
         'min_max_file_size': [10240, 102400],
         'min_max_percent_size_delta': [0, 90]}

    Things were getting messy, handling 1+ incomplete frames at start
    of sequence.  Simplified things (at the cost of increased computation-time)
    by doing 2 passes over the image-frames dictionary
      - process the dict (not handling the special case of first-frames-incomplete)
      - reverse dict and process again (former first frames, if incomplete will
        be correctly FLAGGED as incomplete in this pass; but will get the former
        end-frames wrong, if the are incomplete....)
      - compare possibly_incomplete FLAGS in each, keeping all TRUE values

    The result from each pass may differ at beginning and end, with the incorrect
    entries being FALSE, so keep all TRUE's

    """
    corrupt_frame_data = {
        "corrupt_file_data": {},
        "frame_fill_data": {},
        "min_max_file_size": [],
        "min_max_percent_size_delta": [],
    }

    filename_sizedata_fwd = OrderedDict()
    for filename in image_seq_filenames:
        size = os.path.getsize(os.path.join(src_dir, filename))  # bytes
        filename_sizedata_fwd[filename] = {"size": size}
        # TODO, why not: "= size"?
    # Create the reversed dictionary for the second passes
    filename_sizedata_rev = _reverse_ordered_dict(deepcopy(filename_sizedata_fwd))

    # Collect the image-file data in two passes
    fwd_pass_result = _add_incomplete_file_data(
        filename_sizedata_fwd, config["size_delta_threshold"], config["min_size_bytes"]
    )
    rev_pass_result = _add_incomplete_file_data(
        filename_sizedata_rev, config["size_delta_threshold"], config["min_size_bytes"]
    )
    # Resolve the results from the two passes into the final resolved_result
    filename_sizedata = _resolve_pass_results(fwd_pass_result, rev_pass_result)

    corrupt_frame_data["frame_fill_data"] = _get_frame_fill_data(
        filename_sizedata, config
    )
    corrupt_frame_data["corrupt_file_data"] = _get_incomplete_filename_size(
        filename_sizedata
    )

    min_max_percent_size_delta, min_max_file_size = _get_size_spans(filename_sizedata)
    corrupt_frame_data["min_max_file_size"] = min_max_file_size
    corrupt_frame_data["min_max_percent_size_delta"] = min_max_percent_size_delta
    """
    {'corrupt_file_data': {'image_file.1003.png': 10240, 'image_file.1007.png': 0}, 
    'frame_fill_data': {
        'filler_filename_template': ['image_file', '1001', 'png'], 
        'frame_fill_spec': [(1, '1002'), (1, '1006')]}, 
    'min_max_file_size': [0, 307200], 
    'min_max_percent_size_delta': [0, 100]}
    """
    return corrupt_frame_data


def _get_size_spans(filename_sizedata):
    """
    - min max size and  percent deltas
    Args:
        filename_sizedata:
             OrderedDict([('FB101_122_000_002_0I01_PC002_Z1_AV001.1001.png',
                          {'delta': 0,
                           'percent_delta': 0.0,
                           'possibly_incomplete': False,
                           'size': 10240})
                           }), ...
                        ])

    Returns:

    """
    min_percent_delta = 100
    max_percent_delta = 0
    min_size = 9999999999
    max_size = 0
    for _, sizedata in filename_sizedata.items():
        percent_delta = sizedata["percent_delta"]
        size = sizedata["size"]
        if percent_delta < min_percent_delta:
            min_percent_delta = percent_delta
        if percent_delta > max_percent_delta:
            max_percent_delta = percent_delta
        if size < min_size:
            min_size = size
        if size > max_size:
            max_size = size
    return [round(min_percent_delta), round(max_percent_delta)], [min_size, max_size]


def _add_incomplete_file_data(filename_sizedata, size_delta_threshold, min_file_size):
    """
    Algorithm assumes first frame is complete, then flags frames as incomplete
    if their percent DROP in size is >= size_delta_threshold

    Will produce false negatives (frame not incomplete) at the start of the
    sequence if the first 1+ frames are incomplete.  Algorithm aspects that
    cause this:
    - "possibly_incomplete" bool value is initialized to FALSE (first frame always
       gets "possibly_incomplete = FALSE")
    - Positive-deltas in size (above size_delta_threshold) flip the bool to FALSE
    - Negative-deltas in size (above size_delta_threshold) flip the bool to TRUE

    Args:
        filename_sizedata (OrderedDict):
            e.g.:
            OrderedDict([('FB101_122_000_002_0I01_PC002_Z1_AV001.1001.png',
                          {'size': 10240}),
                          }), ...
                        ])

        size_delta_threshold (int): [0, 100] with default set in config.
            If the percent size-change between adjacent frames is greater
            than size_delta_threshold, the "possibly_incomplete" bool is reset
            (value depending on direction of size-change)

            "percent_delta" is relative to the larger of the two files so
            value will be in range [0, 100]

    Returns:

           OrderedDict([('FB101_122_000_002_0I01_PC002_Z1_AV001.1001.png',
                          {'delta': 0,
                           'percent_delta': 0.0,
                           'possibly_incomplete': False,
                           'size': 10240})
                           }), ...
                        ])
    """
    possibly_incomplete = False
    previous_size = None
    for filename, size_data in filename_sizedata.items():
        current_size = size_data["size"]
        if previous_size is None:
            previous_size = current_size

        # get delta and percent_delta
        delta = current_size - previous_size

        if delta > 0:  # current-size is larger than previous
            try:
                percent_delta = (abs(delta) / current_size) * 100
            except ZeroDivisionError:
                percent_delta = 100
        else:
            try:
                percent_delta = (abs(delta) / previous_size) * 100
            except ZeroDivisionError:
                percent_delta = 100

        if percent_delta > size_delta_threshold:
            # if delta exceeds the threshold AND
            # CHANGE is from larger-to-smaller (i.e. absolute delta is negative)
            # then possibly incomplete
            possibly_incomplete = delta < 0

        # Finally check against MINIMUM FILE SIZE (an absolute measure)
        if current_size < min_file_size:
            possibly_incomplete = True

        filename_sizedata[filename].update(
            {
                "delta": delta,
                "percent_delta": percent_delta,
                "possibly_incomplete": possibly_incomplete,
            }
        )
        # reset "previous_size" for the next iteration
        previous_size = current_size

    return filename_sizedata


def _resolve_pass_results(odict_1, odict_2):
    """
    Compares bool value (key:"possibly_incomplete") for each file between
    odict_1 and odict_2.  Creates a new OrderedDict to collect the
    correct results.

    The "possibly_incomplete" bool values in each dict will only differ at
    the beginning of the sequence, and the incorrect entries will always
    equal FALSE, so keep all TRUE's

    Args:
        odict_1 (OrderedDict): from first pass
        odict_2 (OrderedDict): from second pass

    Returns:
        resolved_result (OrderedDict):
    """
    # Reverse the result from second pass for easy comparison
    odict_2_reversed = _reverse_ordered_dict(deepcopy(odict_2))
    resolved_result = OrderedDict()
    for key, val in odict_1.items():
        if val != odict_2_reversed[key]:
            if val["possibly_incomplete"]:
                resolved_result[key] = val
            else:
                resolved_result[key] = odict_2_reversed[key]
        else:
            resolved_result[key] = val
    return resolved_result


def _get_frame_fill_data(filename_sizedata, config):
    """
    Creates a data-structure for passing to "replace_frames

    Args:
        filename_sizedata (dict):
        config (dict)
    Returns:
        frame_fill_data (dict):
            {"frame_fill_spec":[(-1, '1002'), (1, '1002'), (1, '1008')] ,
            "filler_filename_template": ['FB101_122_000_002_0I01_PC002_Z1_AV001',
                                        '1002',
                                        'png']}

    dropped-frame data: "frame_fill_spec":[<data-tuple>, ...] where,
     <data-tuple> = (<number-of-dropped-frames>, <frame-number-of-adjacent-valid-frame>
    """
    image_frame_filename_pattern = r"\w+\.(\d{4})\." + config["image_seq_file_type"]
    frame_regex = re.compile(image_frame_filename_pattern)

    frame_fill_spec = []
    valid_frames = []  # collect to have the latest valid frame for forward-fill

    consecutive_corrupt_frames = 0
    for filename, sizedata in filename_sizedata.items():
        current_frame = frame_regex.search(filename).group(1)

        if sizedata["possibly_incomplete"]:
            consecutive_corrupt_frames += 1
        else:
            if consecutive_corrupt_frames:
                if valid_frames:  # CASE: "internal" incomplete frames: do forward fill
                    frame_fill_spec.append(
                        (consecutive_corrupt_frames, valid_frames[-1])
                    )
                else:  # CASE: incomplete-start-frames: do back fill
                    frame_fill_spec.append(
                        (-consecutive_corrupt_frames, current_frame)
                    )

            valid_frames.append(current_frame)
            consecutive_corrupt_frames = 0
    # CASE: incomplete-end-frames.  If at this point, consecutive_corrupt_frames is not zero,
    # then some number of frames (equal to consecutive_corrupt_frames) has not been
    # accounted for  - do forward fill
    if consecutive_corrupt_frames and valid_frames:
        frame_fill_spec.append((consecutive_corrupt_frames, valid_frames[-1]))

    # CREATE filler_filename_template, for creating fill-frame-names
    a_filename = next(iter(filename_sizedata.keys()))  # get first key in dict
    filler_filename_template = a_filename.split(".")
    #  'filler_filename_template': ['FB101_122_000_002_0I01_PC002_Z1_AV001', '1002', 'png']

    frame_fill_data = {
        "filler_filename_template": filler_filename_template,
        "frame_fill_spec": frame_fill_spec,
    }
    return frame_fill_data


def _get_incomplete_filename_size(filename_sizedata):
    """

    Args:
        filename_sizedata:

    Returns:
    {'FB101_122_000_002_0I01_PC002_Z1_AV001.1003.png': 10240,
     'FB101_122_000_002_0I01_PC002_Z1_AV001.1004.png': 10240,
     'FB101_122_000_002_0I01_PC002_Z1_AV001.1005.png': 10240,
     'FB101_122_000_002_0I01_PC002_Z1_AV001.1006.png': 10240,
     'FB101_122_000_002_0I01_PC002_Z1_AV001.1007.png': 10240}
    """
    incomplete_filename_size = {}
    for filename, sizedata in filename_sizedata.items():
        if sizedata["possibly_incomplete"]:
            incomplete_filename_size[filename] = sizedata["size"]
    return incomplete_filename_size


def _reverse_ordered_dict(odict):
    """

    Args:
        odict:

    Returns:

    """
    reversed_odict = OrderedDict()
    for _ in range(len(odict)):
        key, val = odict.popitem()
        reversed_odict.update({key: val})
    return reversed_odict

