"""
ffmpeg -i possibly_corrupted.png -loglevel error -f null -
"""
import os
import subprocess
import time
import multiprocessing
from PySide import QQueue

ffmpeg_exe = r"S:\apps\Windows\ffmpeg\4.3.0\bin\ffmpeg.exe"


def _get_image_sequence(root_dir):
    filenames = None
    for (_, _, filenames) in os.walk(root_dir):
        break

    if filenames:
        filtered_result = [f for f in filenames if ".png" in f]
        result = filtered_result
    else:
        result = "Warning: No image files found at: {}".format(root_dir)
    return result




def get_black_frame(src_dir, image_to_check, num, conn):
    threshold = "30"
    png_path = os.path.join(src_dir, image_to_check)
    cmd_list = [ffmpeg_exe, "-i", png_path, "-vf", "blackframe=80", "-f", "null", "-"]
    proc = subprocess.Popen(cmd_list, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE,)
    conn.send(proc.communicate())
    conn.close()


if __name__ == '__main__':
    """
     
    """
    src_dir = r"U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001"
    files = sorted(_get_image_sequence(src_dir))
    pool = multiprocessing.Pool(20)

    parent_conn, child_conn = multiprocessing.Pipe()
    start = time.time()
    jobs = []
    for num in range(len(files)):
        # range(62, 82):, runtime = 49.4779999256
        image_file = files[num]

        if "png" in image_file:
            p = multiprocessing.Process(target=get_black_frame, args=(src_dir, image_file, num, child_conn))
            jobs.append(p)
            p.start()

    for proc in jobs:
        print(parent_conn.recv())
        proc.join()


    end = time.time()
    runtime = end-start
    print(runtime)