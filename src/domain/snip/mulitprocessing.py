"""
most from https://pymotw.com/2/multiprocessing/basics.html
but others from SO, links provided...
"""

import multiprocessing
from worker import imported_worker
from worker import named_worker
from worker import named_service
from multiprocessing import Process, Queue


def worker0():
    """worker function"""
    print('Worker')
    return


def worker1(num):
    """worker function"""
    print('Worker: ', num)
    return


def use_worker0_no_input():
    jobs = []
    for i in range(5):
        p = multiprocessing.Process(target=worker0)
        jobs.append(p)
        p.start()
    jobs = []


def use_worker1_with_input():
    jobs = []
    for i in range(5):
        p = multiprocessing.Process(target=worker1, args=(i,))
        jobs.append(p)
        p.start()


def use_imported_worker():
    jobs = []
    for i in range(5):
        p = multiprocessing.Process(target=imported_worker, args=(i,))
        jobs.append(p)
        p.start()


def naming_processes():
    service = multiprocessing.Process(name='my_service', target=named_service)
    worker_1 = multiprocessing.Process(name='hard worker', target=named_worker)
    worker_2 = multiprocessing.Process(target=named_worker) # use default name "Process-N"

    worker_1.start()
    worker_2.start()
    service.start()

# https://stackoverflow.com/questions/26774781/python-multiple-subprocess-with-a-pool-queue-recover-output-as-soon-as-one-finis/26783779#26783779

from multiprocessing.pool import ThreadPool
import subprocess

def work(sample):
    my_tool_subprocess = subprocess.Popen('mytool {}'.format(sample),shell=True, stdout=subprocess.PIPE)
    line = True
    while line:
        myline = my_tool_subprocess.stdout.readline()
        print(myline)

def using_threadpool():
    all_samples = ["ad0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17"]
    num = None  # set to the number of workers you want (it defaults to the cpu count of your machine)
    tp = ThreadPool(12)
    for sample in all_samples:
        tp.apply_async(work, (sample,))

    tp.close()
    tp.join()


# https://stackoverflow.com/questions/54615502/getting-the-return-value-of-a-function-used-in-multiprocess





#  getting_return_vals
Q = Queue()


def my_func(procnum, return_dict):
    """worker function"""
    print(str(procnum) + " represent!")
    return_dict[procnum] = procnum


if __name__ == '__main__':
    # use_worker0_no_input()
    #use_worker1_with_input()
    # use_imported_worker()
    # naming_processes()
    # using_threadpool()

    #  getting_return_vals

    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    jobs = []
    for i in range(25):
        p = multiprocessing.Process(target=my_func, args=(i, return_dict))
        jobs.append(p)
        p.start()

    for proc in jobs:
        proc.join()
    print(return_dict.values())