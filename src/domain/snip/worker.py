def imported_worker(num):
    """worker function"""
    print('Worker: ', num)
    return


# =================================NAMED WORKERS
import multiprocessing
import time


def named_worker():
    # name is defined when spawning
    # e.g. worker_1 = multiprocessing.Process(name='worker 1', target=named_worker)
    name = multiprocessing.current_process().name
    print(name, 'Starting')
    time.sleep(2)
    print(name, 'Exiting')


def named_service():
    name = multiprocessing.current_process().name
    print(name, 'Starting')
    time.sleep(3)
    print(name, 'Exiting')