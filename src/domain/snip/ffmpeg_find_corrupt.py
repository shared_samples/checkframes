"""
ffmpeg -i possibly_corrupted.png -loglevel error -f null -
"""
import os
import subprocess
import time
from pprint import pprint

ffmpeg_exe = r"S:\apps\Windows\ffmpeg\4.3.0\bin\ffmpeg.exe"


def _get_image_sequence(root_dir):
    filenames = None
    for (_, _, filenames) in os.walk(root_dir):
        break

    if filenames:
        filtered_result = [f for f in filenames if ".png" in f]
        result = filtered_result
    else:
        result = "Warning: No image files found at: {}".format(root_dir)
    return result




def get_corrupt_frame(src_dir, image_to_check, num):
    threshold = "30"
    png_path = os.path.join(src_dir, image_to_check)
    cmd_list = [ffmpeg_exe, "-i", png_path, "-vf", "blackframe=80", "-f", "null", "-"]
    proc = subprocess.Popen(cmd_list, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE,)
    return proc.communicate()


if __name__ == '__main__':
    """
     
    """
    src_dir = r"U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001"
    files = sorted(_get_image_sequence(src_dir))


    start = time.time()
    for num in range(len(files)):
        # range(62, 82):, runtime = 49.4779999256
        image_file = files[num]
        if "png" in image_file:
            result = get_corrupt_frame(src_dir, image_file, num)
            #print("OK: ", result[0])
            result_str = repr(result[1])
            print(result_str)

    end = time.time()
    runtime = end-start
    print(runtime)