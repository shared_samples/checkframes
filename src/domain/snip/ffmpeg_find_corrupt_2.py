"""
ffmpeg -i possibly_corrupted.png -loglevel error -f null -
"""
import os
import subprocess
# import OpenImageIO as oiio
from pprint import pprint

ffmpeg_exe = r"S:\apps\Windows\ffmpeg\4.3.0\bin\ffmpeg.exe"


def _get_image_sequence(root_dir):
    filenames = None
    for (_, _, filenames) in os.walk(root_dir):
        break

    if filenames:
        filtered_result = [f for f in filenames if ".png" in f]
        result = filtered_result
    else:
        result = "Warning: No image files found at: {}".format(root_dir)
    return result


def get_corrupt_frames(src_dir, image_seq_to_check):
    file_sizes = {}
    for file in image_seq_to_check:
        if "png" in file:
            png_path = os.path.join(src_dir, file)
            print("checking: ", png_path)
            cmd_list = [ffmpeg_exe, "-i", png_path, "-loglevel", "error", "-f", "null", "-"]
            return subprocess.check_output(cmd_list, shell=True)


def get_corrupt_frames_1(src_dir, image_to_check, num):
    threshold = "30"
    png_path = os.path.join(src_dir, image_to_check)
    print(num)
    cmd_list = [ffmpeg_exe, "-i", png_path, "-vf", "blackframe=30", "-f", "null", "-"]
    return subprocess.check_output(cmd_list, shell=False)

def get_corrupt_frames_2(src_dir, image_to_check, num, send_end):
    threshold = "30"
    png_path = os.path.join(src_dir, image_to_check)
    print(num)
    cmd_list = [ffmpeg_exe, "-i", png_path, "-vf", "blackframe=30", "-f", "null", "-"]
    send_end.send(subprocess.check_output(cmd_list, shell=True))


if __name__ == '__main__':
    src_dir = r"U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001"
    files = sorted(_get_image_sequence(src_dir))
    subset = files[:2]
    #get_corrupt_frames(src_dir, files)
    import multiprocessing

    jobs = []
    pipe_list = []
    for num in range(5):
        image_file = files[num]
        if "png" in image_file:
            recv_end, send_end = multiprocessing.Pipe(True)
            p = multiprocessing.Process(target=get_corrupt_frames_1, args=(src_dir, image_file, num))
            #p = multiprocessing.Process(target=get_corrupt_frames_2, args=(src_dir, image_file, num, send_end))
            jobs.append(p)
            pipe_list.append(recv_end)
            p.start()

    for proc in jobs:
        proc.join()
    result_list = [x.recv() for x in pipe_list]
    print(result_list)
