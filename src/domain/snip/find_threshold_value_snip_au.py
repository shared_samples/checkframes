import statistics



# if range[0] == 1:
#     print("no search required")

# at this point we now range[0] == 0
quit_threshold = 2


def get_next_range(range):
    result = {"continue": True, "next_range": []}
    direction = "down"

    min = range[0]
    max = range[1]
    mid = int((max - min) / 2) + min
    low_range = [min, mid]
    #print("ITER, check range: {}".format(range))

    if SORTED_DATA[min] == 0 and SORTED_DATA[mid] == 0:
        # print("\t\tOut-of-range [{}, {}] = [0,0]: go up FULL  MIN=mid; MAX=max".format(min, mid))
        direction = "up"
        next_range = [mid, max]

    elif SORTED_DATA[min] == 0 and SORTED_DATA[mid] == 1:
        # print("\t\tIn-range [{}, {}] = [0, 1]: go up HALF with MIN=min + int((mid-min)/2); MAX=mid".format(min, mid))
        direction = "up"
        next_range = [min + int((mid-min)/2), mid]

    elif SORTED_DATA[min] == 1 and SORTED_DATA[mid] == 0:
        # print("\t\tIn-range [{}, {}] = [1, 0]: go down HALF with MIN=min; MAX=max - int((max-min)/2)".format(min, mid))
        direction = "down"
        next_range = [min, mid - int((mid-min)/2)]

    elif SORTED_DATA[min] == 1 and SORTED_DATA[mid] == 1:
        # print("\t\tOut-of-range [{}, {}] = [1, 1]: go down FULL with MIN=DATA_MIN; MAX=min".format(min, mid))
        direction = "down"
        next_range = [DATA_MIN, min]

    # print "\t", low_range, direction, next_range
    quit_condition = next_range[1] - next_range[0]

    if quit_condition <= 1:
        # print "\tquit_condition = {}".format(quit_condition)
        result["continue"] = False

        # print("SORTED_DATA[{}] = {}".format(next_range[0], SORTED_DATA[next_range[0]]))
        # print("SORTED_DATA[{}] = {}".format(next_range[1], SORTED_DATA[next_range[1]]))
        if SORTED_DATA[next_range[0]] == 1 and SORTED_DATA[next_range[1]] == 1:
            if SORTED_DATA[low_range[0]] == 0:
                result["next_range"] = low_range[0]
            else:
                result["next_range"] = low_range[1]
        else:
            if SORTED_DATA[next_range[0]] == 0:
                result["next_range"] = next_range[0]
            else:
                result["next_range"] = next_range[1]
    else:

        result["continue"] = True
        result["next_range"] = next_range

    return result


def get_threshold_index(range):
    go_again, next_range = get_next_range(range).values()
    if go_again:
        print("\nnext range: {}".format(next_range))
        threshold_index = get_threshold_index(next_range)
    else:
        return next_range
    return threshold_index


def run_tst_data():
    global SORTED_DATA
    sorted_data_tests = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    ]
    for SORTED_DATA in sorted_data_tests:
        global DATA_MIN
        DATA_MIN = 0
        DATA_MAX = len(SORTED_DATA)
        range = [DATA_MIN, DATA_MAX]

        threshold_index = get_threshold_index(range)
        print(threshold_index)

if __name__ == '__main__':
    SORTED_DATA = None
    run_tst_data()