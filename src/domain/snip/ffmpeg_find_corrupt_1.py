"""
ffmpeg -i possibly_corrupted.png -loglevel error -f null -
"""
import os
import subprocess
# import OpenImageIO as oiio
from pprint import pprint

ffmpeg_exe = r"S:\apps\Windows\ffmpeg\4.3.0\bin\ffmpeg.exe"


def _get_image_sequence(root_dir):
    filenames = None
    for (_, _, filenames) in os.walk(root_dir):
        break

    if filenames:
        filtered_result = [f for f in filenames if ".png" in f]
        result = filtered_result
    else:
        result = "Warning: No image files found at: {}".format(root_dir)
    return result


def get_corrupt_frames(src_dir, image_seq_to_check):
    file_sizes = {}
    for file in image_seq_to_check:
        if "png" in file:
            png_path = os.path.join(src_dir, file)
            print("checking: ", png_path)
            cmd_list = [ffmpeg_exe, "-i", png_path, "-loglevel", "error", "-f", "null", "-"]
            return subprocess.check_output(cmd_list, shell=True)


def get_corrupt_frame(src_dir, image_to_check, num):
    threshold = "30"
    png_path = os.path.join(src_dir, image_to_check)
    print(num)
    cmd_list = [ffmpeg_exe, "-i", png_path, "-vf", "blackframe=30", "-f", "null", "-"]
    return subprocess.check_output(cmd_list, shell=False)


if __name__ == '__main__':
    src_dir = r"U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001"
    files = sorted(_get_image_sequence(src_dir))
    subset = files[:2]
    #get_corrupt_frames(src_dir, files)
    import multiprocessing

    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    jobs = []
    #
    # for num in range(len(files)):
    for num in range(len(subset)):

        image_file = files[num]
        if "png" in image_file:
            p = multiprocessing.Process(target=get_corrupt_frame, args=(src_dir, image_file, num))
            jobs.append(p)
            p.start()

    for proc in jobs:
        proc.join()
    pprint(return_dict)
    print(return_dict.keys())