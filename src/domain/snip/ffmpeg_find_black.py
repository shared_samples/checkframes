"""
ffmpeg -i possibly_corrupted.png -loglevel error -f null -
"""
import os
import re
import subprocess
import time
from pprint import pprint

ffmpeg_exe = r"S:\apps\Windows\ffmpeg\ffmpeg\4.3.0\bin\ffmpeg.exe"
REGEX = re.compile(".+ pblack:(\d{2}) .+")

def _get_image_sequence(root_dir):
    filenames = None
    for (_, _, filenames) in os.walk(root_dir):
        break

    if filenames:
        filtered_result = [f for f in filenames if ".png" in f]
        result = filtered_result
    else:
        result = "Warning: No image files found at: {}".format(root_dir)
    return result



def get_black_frame(image_path, num):

    cmd_list = [ffmpeg_exe, "-i", image_path, "-vf", "blackframe=80", "-f", "null", "-"]
    print(num)
    proc = subprocess.Popen(cmd_list, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE,)
    return proc.communicate()


if __name__ == '__main__':
    """
     
    """
    src_dir = r"U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001"
    files = sorted(_get_image_sequence(src_dir))
    file_result_string = {}
    # start = time.time()
    for num in range(70, 74):
        # range(62, 82):, runtime = 49.4779999256
        image_file = files[num]

        if "png" in image_file:
            image_path = os.path.join(src_dir, image_file)
            result = get_black_frame(image_path, num)
            result_str = repr(result[1])
            print(result_str)
            match = REGEX.search(result_str)
            try:
                percent_black_pixels = match.group(1)
            except Exception:
                # 'NoneType' object has no attribute 'group'
                percent_black_pixels = "OK"
            file_result_string[image_file] = percent_black_pixels
    pprint(file_result_string)
    # end = time.time()
    # runtime = end-start
    # print(runtime)

    # U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001
    # U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001\FB101_122_000_002_0I01_PC002_Z1_AV001.0064.png