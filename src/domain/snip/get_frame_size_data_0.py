"""


"""
import os
import statistics
from collections import OrderedDict
from copy import deepcopy
from pprint import pprint


def get_frame_size_data(src_dir, image_seq_filenames, size_delta_threshold):
    """

    Args:
        src_dir (string): absolute path of image sequence directory
        image_seq_filenames (list): filenames sorted by frame

    Returns:

    """
    filename_size = {}
    filename_sizedata = OrderedDict()

    for filename in image_seq_filenames:
        size = os.path.getsize(os.path.join(src_dir, filename))
        filename_sizedata[filename] = {"size": size}
        filename_size[filename] = size

    min_size = min(filename_size.values())
    max_size = max(filename_size.values())

    size_of_first_frame = next(iter(filename_size.values()))  # first value from dict

    previous_size = size_of_first_frame  # TODO this could be ZERO
    possibly_incomplete = False
    fix_first_frame = False  # for when first frame is actually possibly-incomplete
    frame_count = 1
    for filename, size_data in filename_sizedata.items():
        current_size = size_data["size"]

        # get delta and percent_delta
        delta = current_size - previous_size
        # TODO handle div by zero
        if delta > 0:  # current is the larger
            percent_delta = (abs(delta) / current_size) * 100
        else:
            percent_delta = (abs(delta) / previous_size) * 100

        if percent_delta > size_delta_threshold:
            if delta < 0:
                possibly_incomplete = True

            else:
                possibly_incomplete = False
                # and the previous frame must have had possibly_incomplete = True
                # but the if current frame is 2nd frame, then the 1st frame
                # incorrectly has possibly_incomplete = True (since loop is initialized
                # that way).... so fix it
                if frame_count == 2:
                    fix_first_frame = True
        filename_sizedata[filename].update({"delta": delta,
                                            "percent_delta": percent_delta,
                                            "possibly_incomplete": possibly_incomplete})
        # reset "previous_size" for the next iteration
        previous_size = current_size
       
        frame_count += 1

    if fix_first_frame:  # ERROR marks frame 1 incomplete for case:incomplete_internal_5span'
        for filename, size_data in filename_sizedata.items():
            filename_sizedata[filename]["possibly_incomplete"] = True
            break


    return min_size, max_size, filename_sizedata



def _get_image_sequence(root_dir):
    result = None
    for (_, _, filenames) in os.walk(root_dir):
        result = filenames
        break
    if not result:
        result = "Warning: No image files found at: {}".format(root_dir)

    return result





if __name__ == '__main__':
    src_dir = r"U:\dropbox\fables\20201105_02\FB101_122_000_002_0I01_PC002_Z1_AV001"
    files = _get_image_sequence(src_dir)

    get_frame_size_data(src_dir, files)