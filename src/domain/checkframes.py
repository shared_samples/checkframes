"""
checkframes controller

"""

# STANDARD IMPORTS
import os
import re

# PACKAGE IMPORTS

try:
    from src.domain.missing_frames import get_missing_frame_data
    from src.domain.corrupt_frames import get_corrupt_frame_data
    from src.domain.replace_frames import replace_frames
    from src.domain.util.human_bytes import HumanBytes
    from src.domain.util.report import get_report
except ModuleNotFoundError:  # for tests
    from domain.missing_frames import get_missing_frame_data
    from domain.corrupt_frames import get_corrupt_frame_data
    from domain.fix_frames import replace_frames
    from domain.util.human_bytes import HumanBytes
    from domain.util.report import get_report
#    from checkframes.domain.black_frames import is_black_frame


def get_sequence_frange_data(src_dir, config):
    """
    For checkframes to detect missing frames at beginning and end of an
    image sequence, it must know the EXPECTED first and last frames ("frange").

    PROBLEM: the code cannot know the expected frange.

    This function returns

    The INTENT is for this data to be presented to USER
    e.g. a message like:

        WARNING: EXISTING Frame Range = 0000-1463.
        If necessary please update to the EXPECTED Frame Range.

    **PROBLEM - could be mitigated if the expected-frange was somehow delivered
    with the render (e.g. Shotgrid data, embedded in "src_dir" name)... Note
    that this code can be used for pre-production renders where frame-range
    may not be known

    Args:
        src_dir (string): absolute path of directory holding an image sequence
        config (dict):

    Returns:
        existing_frange ([string, string]): The first and last frame numbers
            of the image sequence under the directory, "src_dir"
    """
    frange_data = {"existing_frange": [], "errors": []}
    # GET IMAGE SEQUENCE
    ret_val = _get_sorted_sequence_filenames(src_dir, config)

    #    If there are errors while getting the image-sequence
    #    then there's no further processing that can be done
    if ret_val["errors"]:
        frange_data["errors"].append(ret_val["errors"])
    else:
        first_and_last_filenames = [
            ret_val["sequence_filenames"][0],
            ret_val["sequence_filenames"][-1],
        ]
        image_frame_regex = re.compile(config["img_seq_filename_pattern"])
        existing_frange = [
            re.match(image_frame_regex, filename)[1]  # the frame number token
            for filename in first_and_last_filenames
        ]

        if existing_frange[0] and existing_frange[1]:
            frange_data["existing_frange"] = existing_frange
    return frange_data


def checkframes(
    config,
    directory=None,
    frange=None,
    size_delta_threshold=None,
    fill_missing=None,
    replace_corrupt=None,
    reply_method=None,
    dry_run=False,
    running_api=False,
    gui=False,
):
    """
    Detects the following (common) rendering errors.
        Missing frames - frames missing from the render.
        Corrupt frames - frames are black or incomplete.
    Optionally inserts (fills) dropped-frames; controlled by "fill_missing" parameter
    Optionally replaces incomplete-frames; controlled by "replace_corrupt" parameter

    Note that "config" will have project-defaults for these paramerters

    Args:
        config (dict):
        directory (string): Manditory.  Root directory of image-file-sequence
        frange (list or None): Usually None if running checkframes API
             e.g. [1001, 1037]
        size_delta_threshold (int or None): None if running checkframes API
        fill_missing (bool or None):
            Usually None if running checkframes API
        replace_corrupt (bool or None):
            Usually None if running checkframes API
        reply_method:
        dry_run:
        running_api:

    If INCOMPLETE-frames are to be "filled" (replaced by valid frames) then they
    must be filled BEFORE dropped-frames are filled.  Otherwise dropped
    frames could be filled by INCOMPLETE-frames....

     TODO test running check_frames as subprocess
    """

    # Handle Parameters
    src_dir = directory
    #     Fall back to project-defaults (if "fill" parameters not defined)
    if fill_missing is None:
        fill_missing = config["fill_missing_frames"]
    if replace_corrupt is None:
        replace_corrupt = config["replace_corrupt_frames"]

    report = get_report(src_dir, fill_missing, replace_corrupt, config)
    # Prep for command-line feedback
    if not running_api:
        os.system("cls")
        print(f"Running checkframes on {src_dir}")

    # GET IMAGE SEQUENCE
    img_seq_data = _get_sorted_sequence_filenames(src_dir, config)

    #    If there are errors while getting the image-sequence
    #    then there's no further processing that can be done
    if img_seq_data["errors"]:
        report.errors = img_seq_data["errors"]
    else:

        # HANDLE CORRUPT FRAMES
        #     detect (possibly) corrupt frames
        corrupt_frames_data = get_corrupt_frame_data(
            src_dir,
            img_seq_data["sequence_filenames"],
            config,
        )
        """
        {'corrupt_file_data': {'image_file.1003.png': 10240, 'image_file.1007.png': 0}, 
        'frame_fill_data': {
            'filler_filename_template': ['image_file', '1001', 'png'], 
            'frame_fill_spec': [(1, '1002'), (1, '1006')]}, 
        'min_max_file_size': [0, 307200], 
        'min_max_percent_size_delta': [0, 100]}
        """
        # report_data.update(corrupt_frames_data)

        #     replace corrupt frames
        # if config["replace_corrupt_frames"] and not dry_run:
        #     replace_frames_report = replace_frames(
        #         src_dir,
        #         corrupt_frames_data.pop("frame_fill_data"),
        #         config,
        #         backup_filled=config["backup_replaced_files"],
        #         backup_dirname="replaced_incomplete",
        #     )
        #     report_data["replace_corrupt_frames_report"].update(replace_frames_report)

        # HANDLE MISSING FRAMES
        #     detect missing frames
        missing_frame_data = get_missing_frame_data(
            img_seq_data["sequence_filenames"], frange, config
        )
        """
        {'missing_file_data': {
            'image_file.1003.png': '1003', 
            'image_file.1005.png': '1005', 
            'image_file.1007.png': '1007', 
            'image_file.1009.png': '1009', 
            'image_file.1001.png': '1001'}, 
        'frame_fill_data': {
            'frame_fill_spec': [(1, '1002'), (1, '1004'), (1, '1006'), (1, '1008'), (-1, '1002')], 
            'filler_filename_template': ['image_file', '1002', 'png']}}"""
        #     fill-in missing frames
        frame_fill_spec = corrupt_frames_data["frame_fill_data"]["frame_fill_spec"]
        frame_fill_spec += missing_frame_data["frame_fill_data"]["frame_fill_spec"]
        filename_template = missing_frame_data["frame_fill_data"]["filler_filename_template"]
        replace_frames_report = replace_frames(
            src_dir, img_seq_data["sequence_filenames"], frame_fill_spec, filename_template, dry_run, config
        )
        report_data["fill_missing_frames_report"].update(replace_frames_report)
    # TODO
    report_string = _generate_checkframes_report(report_data, config)
    reply = {"report_data": report_data, "report_string": report_string}
    # pprint(report_data["filler_filled_drop_frames"])
    # pprint(report_data["INCOMPLETE_frame_suspects"])

    if reply_method:  # method is in a checkframes UI  (GUI or CLUI
        reply_method(reply)

    # returns to nothing if running checkframes standalone
    # returns to calling app if running checkframes api (as a library)
    # VCAMP, eventually need to alter/augment this return value so VCAMP
    # GUI can use Incomplete-Frames data to assist with handling,
    # manually.... eg. display list of the frames, with ability to
    # select frame to view etc.... perhaps select a subset for filling, then
    # send back here for filling (via VCAMP domain-code)

    return reply


def _get_sorted_sequence_filenames(seq_dir, config):
    """
    Collects files of type "image_seq_file_type" into a list, using a regex for
    name and type validation

    Args:
        seq_dir (string): absolute path to image-sequence files

    Returns:
        ret_val (dict)

    Known error conditions:
    1. There are no files under "seq_dir"
    2. The files under "seq_dir" do not follow the naming spec
    """
    ret_val = {"sequence_filenames": [], "errors": []}

    all_files = os.listdir(seq_dir)
    if not all_files:
        err_msg = f"Error: No image files found at: {seq_dir}\n"
        err_msg += "Please select a directory containing an image sequence"
        ret_val["errors"].append(err_msg)
    else:
        image_frame_regex = re.compile(config["img_seq_filename_pattern"])
        seq_files = [
            re.match(image_frame_regex, file)[0]
            for file in all_files
            if bool(re.match(image_frame_regex, file))
        ]
        if seq_files:
            ret_val["sequence_filenames"] = sorted(seq_files)
        else:
            err_msg = f"Error: Files exist at: {seq_dir}\n"
            err_msg += "...but the filenames don't match the image"
            err_msg += " sequence filename-spec"
            ret_val["errors"].append(err_msg)
    return ret_val


def _generate_checkframes_report(report_data, config):
    """
    Creates readable report messages on the results contained in "report_data"
    Also maintains a data-structure (corrupt_file_data) for use
    in the checkframes_gui to aid manual (gui-aided) visual verification of frames

    Args:
        report_data:
            {'errors': [],
             'fill_missing_frames_report':
                 {'errors': [],
                  'fill_filled_frames': {'FB101_122_000_002_0I01_PC002_Z1_AV001.1006.png':
                                      ['FB101_122_000_002_0I01_PC002_Z1_AV001.1007.png']},
                  'warnings': []},
             'replace_corrupt_frames_report': {},
             'corrupt_file_data': {'FB101_122_000_002_0I01_PC002_Z1_AV001.1001.png': 10240,
                                           'FB101_122_000_002_0I01_PC002_Z1_AV001.1010.png': 10240},
             'min_max_file_size': [10240, 102400],
             'min_max_frame_size': [],
             'min_max_percent_size_delta': [0, 90],
             'warnings': []}
    Returns:

    """
    fill_missing_filled_frames = report_data["fill_missing_frames_report"][
        "fill_filled_frames"
    ]
    fill_corrupt_filled_frames = report_data["replace_corrupt_frames_report"][
        "fill_filled_frames"
    ]
    corrupt_file_data = report_data["corrupt_file_data"]

    report_string = "\ncheckframes Results:"

    report_string += "\n\tDROPPED FRAMES"

    if fill_missing_filled_frames:
        report_string += "\n"
        for _, missing_frames in fill_missing_filled_frames.items():
            for missing_frame in missing_frames:
                report_string += f"\t\t{missing_frame}\n"
    else:
        report_string += " - None\n"

    report_string += "\n\tINCOMPLETE FRAMES"
    if corrupt_file_data:
        report_string += "\n"
        for framename, framesize in corrupt_file_data.items():
            size = HumanBytes.format(framesize, precision=2)
            report_string += f"\t\t{framename}, size: {size}\n"

        min_size = HumanBytes.format(report_data["min_max_file_size"][0], precision=2)
        max_size = HumanBytes.format(report_data["min_max_file_size"][1], precision=2)
        report_string += f"\n\t\tMin/Max Frame Sizes: {min_size}/{max_size}"

        min_percent_delta = report_data["min_max_percent_size_delta"][0]
        max_percent_delta = report_data["min_max_percent_size_delta"][1]
        report_string += (
            f"\n\t\tMin/Max Adjacent Frame "
            f"Size-Change: {min_percent_delta}%/{max_percent_delta}%"
        )
    else:
        report_string += " - None\n"

    if fill_missing_filled_frames or fill_corrupt_filled_frames:
        fill_report_string = "\n\tACTIONS"

        if fill_missing_filled_frames and config["placeholder"]:
            for fill_frame, missing_frames in fill_missing_filled_frames.items():
                fill_report_string += f"\n\tImage File: {fill_frame} "
                fill_report_string += "was used to fill the DROPPED frames:\n"
                for missing_frame in missing_frames:
                    fill_report_string += f"\t\t{missing_frame}\n"

        if fill_corrupt_filled_frames:
            for fill_frame, corrupt_frames in fill_corrupt_filled_frames.items():
                fill_report_string += (
                    f"\n\tImage File: {fill_frame} "
                    f"was used to fill the INCOMPLETE frames:\n"
                )
                for corrupt_frame in corrupt_frames:
                    fill_report_string += f"\t\t{corrupt_frame}\n"

        if fill_report_string != "\n\tACTIONS":
            report_string += fill_report_string
    return report_string


def _get_missing_frames_data_report(src_dst_filenames):
    """

    Args:
        missing_frame_data:

    Returns:

    """
    # pprint(src_dst_filenames)
    report_strings = []

    for src_filename, dst_filenames in src_dst_filenames.items():
        rpt_string = f"Image-File {src_filename}\nhas been "
        rpt_string += "duplicated as 'stand-ins' for the dropped frames:\n"
        for dst_filename in dst_filenames:
            rpt_string += f" - {dst_filename}\n"

        report_strings.append(rpt_string)
    return report_strings
