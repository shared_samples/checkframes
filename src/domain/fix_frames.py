# pylint: disable=E0401  # import-error
"""replace_frames.py

"""
# STANDARD IMPORTS
import os
from pprint import pprint

# PACKAGE IMPORTS
try:  # when testing
    import data.filesystem_service as filesystem
except ModuleNotFoundError:  # when deployed
    import src.data.filesystem_service as filesystem


def replace_frames(src_dir, img_seq_filenames, frame_fill_spec, filename_template, dry_run, config):
    """

    Args:
        src_dir (string): absolute path to image sequence files
        frame_fill_spec :[(-1, '1002'), (1, '1002'), (1, '1008')]
            "filler_filename_template": ['FB101_122_000_002_0I01_PC002_Z1_AV001',
                                        '1002',
                                        'png']}

        dry_run (bool):
        config (dict):
        backup_filled (bool):
        backup_dirname:

    Returns:


    writes "fixed" image-seq to subdir of src_dir
    """
    ret_val = {"errors": [], "warnings": [], "frame_replacement_map": {}}

    src_dst_filenames = _get_src_dst_filenames(img_seq_filenames, frame_fill_spec, filename_template )

    dst_dir = os.path.join(src_dir, config["fixed_img_seq_dirname"])

    for src_filename, dst_filenames in src_dst_filenames.items():
        for dst_filename in dst_filenames:
            if not dry_run:
                err = filesystem.copy_file(
                    os.path.join(src_dir, src_filename),
                    os.path.join(dst_dir, dst_filename),
                )
            else:
                err = []
            if err:    # [err_msg, src_path, dst_path]
                ret_val["errors"].append(err)
            else:
                ret_val["frame_replacement_map"] = src_dst_filenames

    return ret_val


def _get_src_dst_filenames(img_seq_filenames, frame_fill_spec, filename_template):
    """

    The "src" filenames will be an existing image-frame, adjacent to a SPAN
    of missing image-frames; "dst" filenames are the names of the
    missing image-frame files, and the

    Args:
       frame_fill_spec :[(-1, '1002'), (1, '1002'), (1, '1008')]

            where,
            <frame-fill-spec> (list of tuples): [(span, frame_number_of_existing_frame), ...]
                - span (int): number of missing frames
                    - Positive: do forward fill
                    - Negative: to back fill (handles case of dropped frames at START of frange)
                - frame_number_of_existing_frame (string): number of frame to use when filling dropped frames
                e.g. [(-2, '0002), (2, '0036'), (3, '0089')]

            <filler-filename-template> (list of strings):
                [<image_sequence_base_filename>,
                <placeholder-for-the-frame-to-create>,
                <image-seq-filetype>]

                Used to create the names of filler-image-files


    Returns:
        src_dst_filenames (dict): {<src-frame-filename>: [<dst-frame-filename>, ...], ... }

            <src-frame-filename> is the name of the "filler-frame" used to
            create image-files that are missing from the image-sequence

            <dst-frame-filename>, created here, the name(s) of the copies of the
            source-frame-file

            e.g. {'FB101_122_000_002_0I01_PC002_Z1_AV001.1002.png':
                  ['FB101_122_000_002_0I01_PC002_Z1_AV001.1001.png'],
                 'FB101_122_000_002_0I01_PC002_Z1_AV001.1009.png':
                 ['FB101_122_000_002_0I01_PC002_Z1_AV001.1010.png']}

            here, 1002 backward-fills 1001 and 1009 forward-fills 1010
    """
    src_dst_filenames = {}
    for spec in frame_fill_spec:
        frm_span = spec[0]
        frm_num = spec[1]

        frm_increment = 1
        if frm_span < 0:
            frm_increment = -1

        src_filename = f"{filename_template[0]}.{frm_num}.{filename_template[2]}"
        # src_path = os.path.join(src_dir, src_filename)
        src_dst_filenames[src_filename] = []

        increment = frm_increment
        for i in range(abs(frm_span)):
            fill_frm_num = str(int(frm_num) + increment).zfill(4)
            increment += frm_increment
            dst_filename = f"{filename_template[0]}.{fill_frm_num}.{filename_template[2]}"
            # dst_path = os.path.join(src_dir, dst_filename)
            src_dst_filenames[src_filename].append(dst_filename)

    # ADD successful frames to src_dst_filenames
    #    any filaname in img_seq_filenames that is NOT in any dst filenames
    #    that are currently in srd_dst_filenames (i.e., the dst filenames that
    #    are to replace the problem files...)
    all_dst_filenames = []
    for src_filename, dst_filenames in src_dst_filenames.items():
        for dst_filename in dst_filenames:
            all_dst_filenames.append(dst_filename)

    for filename in img_seq_filenames:
        if filename not in all_dst_filenames:
            if filename not in src_dst_filenames.keys():
                src_dst_filenames[filename] = []
            src_dst_filenames[filename].append(filename)
    pass
    """drop_internal_isolated_2span_5span
    {'image_file.1001.png': ['image_file.1002.png', 'image_file.1003.png', 'image_file.1001.png'], 
    'image_file.1004.png': ['image_file.1005.png', 'image_file.1006.png', 'image_file.1007.png', 'image_file.1008.png', 'image_file.1009.png', 'image_file.1004.png'], 
    'image_file.1010.png': ['image_file.1010.png']}
    """
    return src_dst_filenames
